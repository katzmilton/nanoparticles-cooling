from sim_functions import *
from analysis_functions import *
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as spc

name = Path(__file__).stem 

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 
x0 = [[1e-4,1e-4,1e-4]]
# anisotropy = np.arange(0,0.6,0.1)
anisotropy = [0.5,0.7,0.9,1.0]

ions = {'mass': mass, 'charge': charge,'initial_positions':x0} 
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'alpha':0}

Tavg_final = []

laser = {}

for a in range(len(anisotropy)):
    trap.update(anisotropy=anisotropy[a])


    time,positions,_ = linear_lasercool_model(name,ions,trap,laser,2e5)
# np.savez(f'resultados_damp_{damp[d_i]:.0e}',time=time,positions=positions,velocities=velocities,ions = ions)

    t = time
    x = positions[:,0,0]
    y = positions[:,0,1]
    z = positions[:,0,2]

    freqx = frequencies(x,t[1]-t[0])
    freqy = frequencies(y,t[1]-t[0])
    freqz = frequencies(z,t[1]-t[0])

    print(f'{freqx=}')
    print(f'{freqy=}')
    print(f'{freqz=}')
    print(f"{trap['frequency']=}")

    fig_pos,[ax_x,ax_y,ax_z] = plt.subplots(3,sharex = True)
    # fig_pos.tight_layout()
    ax_x.set_ylabel('X (m)')
    ax_y.set_ylabel('Y (m)')
    ax_z.set_ylabel('Z (m)')
    ax_z.set_xlabel('Tiempo (s)')
    ax_x.ticklabel_format(axis ='y',style='scientific',scilimits = (0,0))
    ax_y.ticklabel_format(axis = 'y',style='scientific',scilimits = (0,0))
    ax_z.ticklabel_format(axis = 'y',style='scientific',scilimits = (0,0))
    fig_pos.set_figsize=[14,5]
    ax_x.plot(t,x)
    ax_y.plot(t,y)
    ax_z.plot(t,z)
# pos_theory = 1e-4*np.cos(np.pi*trap['frequency']*np.sqrt(a[0] + q[0]**2/2)*t)*(1-q[0]*np.cos(2*np.pi*trap['frequency']*t))
# ax_x.plot(t,pos_theory,label= 'Teoría')

    fig_freqs,[ax_fx,ax_fy,ax_fz] = plt.subplots(3,sharex=True)
    ax_fx.plot(np.fft.rfftfreq(len(x),time[1]-time[0]),abs(np.fft.rfft(x)))
    ax_fy.plot(np.fft.rfftfreq(len(y),time[1]-time[0]),abs(np.fft.rfft(y)))
    ax_fz.plot(np.fft.rfftfreq(len(z),time[1]-time[0]),abs(np.fft.rfft(z)))
    ax_fx.set_xlim([0,4e6])
    ax_fz.set_xlabel('Frecuencia (Hz)')

# print(f"{len(x.shape)=}")

# plt.show()

    textfile = open('datos.txt','a')
    textfile.write(f'{freqx=}\n')
    textfile.write(f'{freqy=}\n')
    textfile.write(f'{freqz=}\n')
    textfile.write(f"{trap['frequency']=}\n")
    textfile.close

    fig_pos.savefig(f"positions_anisotropy_{anisotropy[a]:.2}.pdf")
    fig_pos.savefig(f"positions_anisotropy_{anisotropy[a]:.2}.png",dpi=600)
    fig_freqs.savefig(f"frequencies_anisotropy_{anisotropy[a]:.2}.png",dpi=600)
    fig_freqs.savefig(f"frequencies_anisotropy_{anisotropy[a]:.2}.svg")
    
    fig_pos.clf()
    fig_freqs.clf()
plt.show()
