from sim_functions import *
from analysis_functions import *
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as spc

name = Path(__file__).stem 

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 
x0 = [[1e-4,1e-4,1e-4]]
qvar = [1,5/3,6]

ions = {'mass': mass, 'charge': charge, 'initial_positions': x0} 
t = []
x = []
y = []
z = []
fig,ax= plt.subplots(2,3,figsize = (0.45*8,0.45*5))
ax[0,0].set_ylabel('X (m)')
ax[0,1].set_xlabel('Tiempo (s)')
ax[1,1].set_xlabel('Frecuencia (Hz)')
ax[1,0].set_ylabel('Amplitud (u.a.)')
for i in range(len(qvar)):
    trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
            'frequency': trap_freq, 'voltage': qvar[i]*300, 'endcapvoltage': 15,'anisotropy' : 0.9,'alpha': 0}

    Tavg_final = []

    laser = {}

    time,positions,velocities = linear_lasercool_model(name,ions,trap,laser,1e6)
    # np.savez(f'resultados_damp_{damp[d_i]:.0e}',time=time,positions=positions,velocities=velocities,ions = ions)

    t.append(time)
    x.append(positions[:,0,0])
    y.append(positions[:,0,1])
    z.append(positions[:,0,2])

    freqx = frequencies(x[-1],t[-1][1]-t[-1][0])
    freqy = frequencies(y[-1],t[-1][1]-t[-1][0])
    freqz = frequencies(z[-1],t[-1][1]-t[-1][0])

# print(f'{freqx=}')
# print(f'{freqy=}')
# print(f'{freqz=}')
# print(f"{trap['frequency']=}")

    a,q = aq_coefficients(ions,trap)

    textfile = open('datos.txt','a')
    textfile.write(f'{freqx=}\n')
    textfile.write(f'{freqy=}\n')
    textfile.write(f'{freqz=}\n')
    textfile.write(f"{trap['frequency']=}\n")
    textfile.write(f"{a=}\n")
    textfile.write(f"{q=}\n")
    textfile.write(f"{trap['frequency']*np.sqrt(a+0.5*q**2)/2=}\n\n")
    textfile.close
    # print(f'{a=}')
    # print(f'{q=}')
    # print(f"{trap['frequency']*np.sqrt(a + 0.5*q**2)/2=}")

    pos_theory = 1e-4*np.cos(np.pi*trap['frequency']*np.sqrt(a[0] + q[0]**2/2)*t[-1])*(1-q[0]*np.cos(2*np.pi*trap['frequency']*t[-1]))
# fig_pos.set_size_inches(8,4.8)
    ax[0,i].plot(t[-1],x[-1],label = f'Simulación')
    ax[0,i].text(1.2e-5,0.5e-4,f'$q_x$={q[0]:.2}',size = 14)
    ax[0,i].plot(t[-1],pos_theory,label = 'Teoría')
# ax_y.plot(t,y)
# ax_z.plot(t,z)

# fig_freqs,[ax_fx,ax_fy,ax_fz] = plt.subplots(3,sharex=True)
    ax[1,i].plot(np.fft.rfftfreq(len(x[-1]),time[1]-time[0]),abs(np.fft.rfft(x[-1])))
    ax[1,i].plot([freqx]*2,[0,max(abs(np.fft.rfft(x[-1])))],ls = '--',label='frecuencia esperada')
    ax[1,i].set_xlim([0,1e7])
# ax_fy.plot(np.fft.rfftfreq(len(y[-1]),time[1]-time[0]),abs(np.fft.rfft(y[-1])))
# ax_fz.plot(np.fft.rfftfreq(len(z[-1]),time[1]-time[0]),abs(np.fft.rfft(z[-1])))

# print(f"{len(x.shape)=}")

# plt.show()

ax[0,2].legend()
ax[1,2].legend()
fig.set_size_inches(15, 7)
plt.tight_layout()

fig.savefig(f"trampa.svg")
plt.show()
