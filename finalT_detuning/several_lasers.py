from myfuncs import *
import matplotlib.pyplot as plt

name = 'several_lasers'

long_onda = 1e-6

k = [2*np.pi/long_onda/np.sqrt(3)]*3

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 

gamma = 80e6
# detuning = [(gamma/2)*i for i in np.arange(0.6,1.4,0.4)]
detuning = [gamma/2,5*gamma,20*gamma]

ions = {'mass': mass, 'charge': charge,'initial_positions': [[1e-3*(np.sin(np.pi/8)+np.cos(np.pi/8)),1e-3,1e-3*(np.sin(np.pi/8)-np.cos(np.pi/8))]]}

trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha': np.pi/8, 'domain': [5e-3,5e-3,5e-3]}
laser = [{'gamma': gamma, 'k': k, 'detuning': det_i} for det_i in detuning]

time,positions,velocities = dopplercooling(name,ions,trap,laser,5e8)

np.savez(f'resultados_laser_cooling_detunings_{detuning[0]:.2e},{detuning[1]:.2e},{detuning[2]:.2e}',time= time, positions= positions, velocities= velocities,ions = ions) 

##
# fig,ax= plt.subplots(3,sharex =True)
# ax[0].plot(time[::100],positions[::100,0,0])
# ax[1].plot(time[::100],positions[::100,0,1])
# ax[2].plot(time[::100],positions[::100,0,2])
# ax[2].set_xlabel('Time (s)')
# ax[0].set_ylabel('X (m)')
# ax[1].set_ylabel('Y (m)')
# ax[2].set_ylabel('Z (m)')
# ax[0].grid()
# ax[1].grid()
# ax[2].grid()
# ax[0].set_title(f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}')
##

plt.show()
