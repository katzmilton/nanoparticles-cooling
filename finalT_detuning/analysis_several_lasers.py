##
import numpy as np
from matplotlib import pyplot as plt
from myfuncs import *
from scipy import signal as sg
import ruptures as rpt
import scipy as sp

gamma = 80e6
detuning = [gamma/2,gamma*5,gamma*20]

print('leyendo resultados')
resultados = dict(np.load(f'resultados_laser_cooling_detunings_{detuning[0]:.2e},{detuning[1]:.2e},{detuning[2]:.2e}.npz',allow_pickle = True))

ions = resultados.get('ions',np.array({'mass': 40})).item()
macro_freq = min([frequencies(resultados['positions'][:,0,d],resultados['time'][1]-resultados['time'][0])[0] for d in range(3)])

steps_per_period = int((1/macro_freq)/(resultados['time'][1]-resultados['time'][0]))

print('calculando temperatura')
temp = measure_temperature(resultados['velocities'],50*steps_per_period,ions['mass'])[0]
resultados.update(temperature = temp)
print('guardando temperatura')
np.savez(f'resultados_laser_cooling_detunings_{detuning[0]:.2e},{detuning[1]:.2e},{detuning[2]:.2e}.npz',**resultados)
 
print('graficando temperatura')
fig_T,ax_T = plt.subplots()
ax_T.plot(resultados['time'][:len(temp)],temp)
ax_T.set_xlabel('Time (s)')
ax_T.set_ylabel('Temperature (k)')
ax_T.grid()
fig_T.savefig(f'temperatura_detunning_{detuning[0]:.2e},{detuning[1]:.2e},{detuning[2]:.2e}.png')

model = 'l2'
print('empiezo binseg')
algo = rpt.Binseg(model=model).fit(np.diff(resultados['temperature'])/np.diff(resultados['time'][:len(resultados['temperature'])]))
result = algo.predict(n_bkps=5)
print('termino binseg')
print(len(result))
# result = [100*res for res in result]
result.insert(0,0)


fig_treg,ax_treg = plt.subplots()

slopes_list = []

reg_num = np.arange(len(result))
for i in range(1,len(result)):
    t_i = resultados['time'][result[i-1]:result[i]]
    T_i = resultados['temperature'][result[i-1]:result[i]]
    slope, intercept, _, _, std_err = sp.stats.linregress(t_i,T_i)
    slopes_list.append(slope)

    ax_treg.plot(resultados['time'][result[i-1]:result[i]],slope*resultados['time'][result[i-1]:result[i]] + intercept,color = 'green',lw = 3)
    ax_treg.text(resultados['time'][result[i-1]],70-10*i,str(reg_num[i-1]),size = 22)

f = open('datos.txt','a')
f.write(f'Simulación con detunings = {detuning} \n')
f.write(f"tiempos de cambio \t {resultados['time'][result]}\n")
f.write(f'Pendientes \t {slopes_list}')
f.write(f"T final \t {np.mean(resultados['temperature'][result[-2]+100:result[-1]])}\n\n")

print('graficando')

ax_treg.plot(resultados['time'][:len(resultados['temperature'])],resultados['temperature'])
color = ['b','r']*len(result)
for i in range(1,len(result)):
    ax_treg.fill_between([resultados['time'][result[i-1]],resultados['time'][result[i]]],[0,0],[max(resultados['temperature']),max(resultados['temperature'])],color = color[i],alpha = 0.3,interpolate = True)
ax_treg.grid()
ax_treg.set_xlabel('time (s)')
ax_treg.set_ylabel('temperature(k)')
fig_treg.savefig(f'temperature_regions_{detuning[0]:.2e}_{detuning[1]:.2e},{detuning[2]:.2e}.png')

plt.show()
