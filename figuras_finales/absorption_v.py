import numpy as np
import scipy.constants as spc
import matplotlib.pyplot as plt


long_onda = 397e-9
k = np.array([2*np.pi/long_onda/np.sqrt(3)]*3)
gamma = 23e6
ratio = np.array([0.5,5,10])
v = np.linspace(-70,15,1000)

fig,ax = plt.subplots()
[ax.plot(v,spc.hbar*k[0]*gamma*0.5/(1+1+(2*ratio[d]+k[0]*v/gamma)**2),zorder = 2,label=f'$\Delta$ = {ratio[d]} $\Gamma$') for d in range(3)]
ax.plot([0,0],[0,6e-21],ls = '--',color = 'k',alpha = 0.5,zorder = 1)
# ax.plot(v,spc.hbar*k[0]*gamma*0.5/(1+1+(2*ratio[0])**2)*(1-8*k[0]*ratio[0]/(gamma*(1+1+(2*ratio[0])**2))*v))
ax.set(xlabel='$v_x$ (m/s)',ylabel = '$F_x$ (N)')
ax.legend(loc = 2)
plt.tight_layout()
fig.savefig('force_velocities.svg')
fig.savefig('force_velocities.png',dpi=800)
plt.show()
