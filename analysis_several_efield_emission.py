import numpy as np
from matplotlib import pyplot as plt
from analysis_functions import *
import scipy.constants as spc

gamma = 80e6

print('leyendo resultados')
filename = 'resultados_efield_emission_1000particles_noinitpos.npz'
filename_field = 'resultados_efield_emission_False.npz'
resultados = load_data(filename)
resultados_field = load_data(filename_field)
ions = resultados['ions']
trap = resultados['trap']
# a,q = aq_coefficients(ions,trap)
# macro_freq = 2*np.pi*trap['frequency']*np.sqrt(a**2 + q**2/2)/2
macro_freq = [frequencies(resultados['velocities'][:,0,d],resultados['time'][1]-resultados['time'][0])[0] for d in range(3)]
steps_per_period = int(1/(min(macro_freq)*(resultados['time'][1] - resultados['time'][0])))

momentum_resta = resultados['velocities']*0
velocities_filtro = resultados['velocities']*0
for i in range(resultados['velocities'].shape[1]):
    momentum_resta[:,i,:] = ions['mass']*1.66e-27*(resultados['velocities'][:,i,:] - resultados_field['velocities'][:,0,:])/(spc.hbar*2*np.pi*1e6)

    for d in range(3):
        _,velocities_filtro[:-1,i,d] = bandpass_filter(resultados['velocities'][:,i,d],macro_freq[d],macro_freq[d]/10,resultados['time'][1] - resultados['time'][0])

momentum_filtro = ions['mass']*1.66e-27*velocities_filtro/(spc.hbar*2*np.pi*1e6)

p2_avg_resta = np.mean(np.var(momentum_resta[-2,:,0])+np.var(momentum_resta[-2,:,1])+np.var(momentum_resta[-2,:,2]))
p2_avg_filtro = np.mean(np.var(momentum_filtro[-2,:,0])+np.var(momentum_filtro[-2,:,1])+np.var(momentum_filtro[-2,:,2]))
p2_avg_gas = ions['mass']*1.66e-27*np.mean(np.sum(resultados['velocities'][-1,:,:]**2,axis=1))/(spc.hbar*2*np.pi*1e6)
p = 1 - np.exp(-4.4e-9 * gamma)

f = open('datos.txt','a')
f.write(f"simulacion con {resultados['velocities'].shape[1]} particulas\n")
f.write(f'promedio entre las velocidades cuadráticas restando el campo: {p2_avg_resta/(p*1e6):.2}\n')
f.write(f'promedio entre las velocidades cuadráticas filtrando el campo: {p2_avg_filtro/(p*1e6):.2}\n')
f.write(f"promedio las 1000 particulas como un gas: {p2_avg_gas/(p*1e6)}\n")
f.close()

i = np.random.randint(0,99)
fig_vel, ax_v = plt.subplots(3,sharex=True)
[ax_vi.plot(resultados['time'],resultados['velocities'][:,i,d]) for d,ax_vi in enumerate(ax_v)]
ax_v[0].set_title(f'velocidad de la particula {i}')
plt.show()
