from sim_functions import *
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as spc

name = Path(__file__).stem 

T_final = np.logspace(-4,-1,4)
damp = [1e-4]

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 


ions = {'mass': mass, 'charge': charge} 
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 300, 'endcapvoltage': 15,'anisotropy' : 0.9}


##
for T_i in range(len(T_final)):
    for d_i in range(len(damp)):
        try:
            laser = {'damp': damp[d_i],'T_fin':T_final[T_i]}
            trap.update(pseudo=False)
            time,positions,velocities = linear_lasercool_model(name,ions,trap,laser,1e8)
            np.savez(f'resultados_damp_{damp[d_i]}_Tfinal_{T_final[T_i]}.npz',time = time,positions = positions,velocities=velocities,ions = ions,trap = trap)

            trap.update(pseudo=True)
            time,positions,velocities = linear_lasercool_model(name,ions,trap,laser,1e7)
            np.savez(f'resultados_damp_{damp[d_i]}_Tfinal_{T_final[T_i]}_pseudo.npz',time = time,positions = positions,velocities=velocities,ions = ions,trap = trap)

        except:

            f = open(f"errores.txt", "a") 
            f.write(f'Simulation with T = {T_final[T_i]} and damp = {damp[d_i]}')
            f.write("\nError during simulation\n\n")
            f.close


