import numpy as np
from matplotlib import pyplot as plt
from analysis_functions import *
import scipy.constants as spc

gamma = 80e6
detuning = np.array([1,5,10,15,20])*gamma/2
# detuning = [gamma/2]

Tfinal_macro = []
Tfinal= []
Tstd_macro = []
Tstd= []
detuning_working = []

for det_i in range(len(detuning)):
    try:
        print(f'{det_i=}')
        filename = f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}_{det_i}.npz'
        resultados = load_data(filename)

        trap = resultados['trap']
        ions = resultados['ions']

        a,q = aq_coefficients(ions,trap)

        macro_freq = np.sqrt(a + q**2 / 2) * trap['frequency']/2
        macro_spp = int((1/min(macro_freq))/(resultados['time'][1]-resultados['time'][0]))
        micro_freq = trap['frequency']
        micro_spp = int((1/micro_freq)/(resultados['time'][1]-resultados['time'][0]))

        filter_freq = (max(macro_freq) + micro_freq)/2

        print(f'{filter_freq=}')

        v = resultados['velocities'][:,0,:]
        vx = resultados['velocities'][:,0,0]
        vy = resultados['velocities'][:,0,1]
        vz = resultados['velocities'][:,0,2]

        vx_macro,vx_micro = lowpassfilter(v[:,0],filter_freq,resultados['time'][1] - resultados['time'][0])
        vy_macro,vy_micro = lowpassfilter(v[:,1],filter_freq,resultados['time'][1] - resultados['time'][0])
        vz_macro,vz_micro = lowpassfilter(v[:,2],filter_freq,resultados['time'][1] - resultados['time'][0])

        # plt.plot(resultados['time'][:-1],vx_macro,alpha = 0.4,label='macro')
        # plt.plot(resultados['time'][:-1],vx_micro,alpha=0.4,label='micro')
        # plt.legend()
        temp_macro = measure_temperature(np.array([vx_macro,vy_macro,vz_macro]).T,macro_spp,ions['mass'])[0]
        temp= measure_temperature(np.array([vx,vy,vz]).T,micro_spp,ions['mass'])[0]

        Tfinal_macro.append(np.mean(temp_macro[-1000*macro_spp:]))
        Tfinal.append(np.mean(temp[-1000*micro_spp:]))
        Tstd_macro.append(np.std(temp_macro[-1000*macro_spp:],ddof=1)/np.sqrt(len(temp_macro[-1000*macro_spp:])))
        Tstd.append(np.std(temp[-1000*micro_spp:],ddof=1)/np.sqrt(len(temp[-1000*micro_spp:])))
        detuning_working.append(det_i)

        print('graficando temperatura')
        fig_T,ax_T = plt.subplots()
        print(temp.shape)
        ax_T.semilogy(resultados['time'][:len(temp_macro)],temp_macro)
        ax_T.semilogy(resultados['time'][:len(temp)],temp)
        ax_T.semilogy(resultados['time'][-1000*macro_spp:],Tfinal_macro[-1]*np.ones(1000*macro_spp))
        ax_T.semilogy(resultados['time'][-1000*micro_spp:],Tfinal[-1]*np.ones(1000*micro_spp))
        ax_T.set_xlabel('Time (s)')
        ax_T.set_ylabel('Temperature (k)')
        ax_T.grid()
        fig_T.savefig(f'temperatura_macromicro_detunning_{detuning[det_i]:.2e}_{det_i}.png')
    except:
        print('failed')

delta = np.linspace(0.1,20,100)*gamma/2
saturation = 1

T_theory_w =spc.hbar*gamma**2*(1+saturation+(2*delta/gamma)**2)**2/(48*saturation*delta*spc.k)

fig_Tf,ax_Tf = plt.subplots()
fig_Tf.set_figwidth(8)
ax_Tf.errorbar(np.array(detuning)[detuning_working],np.array(Tfinal), yerr = np.array(Tstd), marker = 'o',ls= ' ',label = 'resultados')
ax_Tf.errorbar(np.array(detuning)[detuning_working],np.array(Tfinal_macro), yerr = np.array(Tstd_macro), marker = 'o',ls= ' ',label = 'sin micromoción')
ax_Tf.plot(delta,T_theory_w,label = 'predicción teórica wineland')
ax_Tf.set(yscale = 'log')
ax_Tf.set_xlabel('detuning (Hz)')
ax_Tf.set_ylabel('Final temperature (k)')
ax_Tf.grid()
ax_Tf.legend()
fig_Tf.savefig('detuning_temp_withtheory.svg')
plt.show()
