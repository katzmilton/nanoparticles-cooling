from myfuncs import *
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as spc

name = Path(__file__).stem 

T_final = np.logspace(-4,-1,4)
damp = np.logspace(-2,-1,1)

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 


ions = {'mass': mass, 'charge': charge, 'initial_positions': [[1e-4*(np.sin(np.pi/8)+np.cos(np.pi/8)),1e-4,1e-4*(np.sin(np.pi/8)-np.cos(np.pi/8))]]} 
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9,'alpha': np.pi/8}


##
for T_i in range(len(T_final)):
    for d_i in range(len(damp)):
        try:
            laser = {'damp': damp[d_i],'T_fin':T_final[T_i]}
            time,positions,velocities = linear_lasercool_model(name,ions,trap,laser,5e7)
            np.savez(f'resultados_damp_{damp[d_i]}_Tfinal_{T_final[T_i]}.npz',time = time,positions = positions,velocities=velocities,ions = ions)

        except:

            f = open(f"errores.txt", "a") 
            f.write(f'Simulation with T = {T_final[T_i]} and damp = {damp[d_i]}')
            f.write("\nError during simulation\n\n")
            f.close


