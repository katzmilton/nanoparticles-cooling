import numpy as np
from matplotlib import pyplot as plt
from analysis_functions import *
import scipy.constants as spc

gamma = 0.3e12
detuning = [gamma/2]
# detuning = np.array([5,10,15,20])*gamma/2
# detuning = np.array([0.5,1,5,10])*gamma/2
nperiods = 100

Tfinal = []
Tfinal_ps = []
Tstd = []
Tstd_ps = []
detuning_working = []
detuning_working_ps = []

# mass = ['500','5000','50000','5e5']

for det_i in range(len(detuning)):
# try:
        print(det_i)
        resultados= load_data(f'resultados_nanoparticles_detuning_{detuning[det_i]:.2e}.npz')
        ions = resultados['ions']
# laser = resultados['laser'].item()
        trap = resultados['trap']
        saturation = 1
# correlation_time = gamma**2*ions['mass']*1.66e-27*(1+saturation+(2*detuning[det_i]/gamma)**2)**2/(2*spc.hbar*ions['N_ions']*laser['decay_rate']*saturation*detuning[det_i]*laser['k'][0]**2)
# print(correlation_time)

# steps_per_corrtime = int(correlation_time/(resultados['time'][1]-resultados['time'][0]))
# print(steps_per_corrtime)
        macro_freq = min([frequencies(resultados['positions'][:,0,d],resultados['time'][1]-resultados['time'][0])[0] for d in range(3)])

        steps_per_period = int((1/macro_freq)/(resultados['time'][1]-resultados['time'][0]))

        temp = measure_temperature(resultados['velocities'],nperiods*steps_per_period,ions['mass'])[0]
# T,std = final_temperature(resultados['velocities'],steps_per_corrtime,ions['mass'],std=False)
# Tfinal.append(T[0])
    # Tfinal_ps.append(final_temperature(resultados_ps['velocities'],50*steps_per_period*1*steps_per_period,ions['mass']))
# Tstd.append(std[0])
        detuning_working.append(det_i)

        print('graficando temperatura')
        fig_T,ax_T = plt.subplots()
        ax_T.semilogy(resultados['time'][2:len(temp)],temp[2:])
# ax_T.semilogy(resultados['time'][steps_per_corrtime:],Tfinal[-1]*np.ones(len(resultados['time'][steps_per_corrtime:])))
        ax_T.set_xlabel('Tiempo (s)')
        ax_T.set_ylabel('Temperatura (k)')
    # fig_v,ax_v = plt.subplots()
    # ax_v.plot(resultados['time'],resultados['velocities'][:,0,0])
    # fig_T.savefig(f'temperatura_detunning_{detuning[det_i]:.2e}_{det_i}.png')
    # fig_T.savefig(f'temperatura_detunning_{detuning[det_i]:.2e}_{det_i}.svg')

        print(ions)
# except:
        print(f'failed {detuning[det_i]} complete')
##
delta = np.linspace(0.1,20,100)*gamma/2
a,q = aq_coefficients(ions,trap)
beta = np.sqrt(a+q**2/2)
print(f'{a=}')
print(f'{q=}')
print(f'{beta=}')

alpha = (1 + (0.8*q/(beta))**2 * (beta**2/4 +1))
rho_ee = (saturation/2)/(1+saturation+(2*delta/gamma)**2)

T_theory_w_ps = spc.hbar*gamma**2*rho_ee*(1+saturation+(2*delta/gamma)**2)**2/(8*saturation*delta*spc.k)
# T_theory_w = spc.hbar*gamma**2*rho_ee*(1+saturation+(2*delta/gamma)**2)**2/(8*saturation*delta*spc.k)
# deltaT = T_theory_w*np.sqrt(2)/np.sqrt(1)

# fig_Tf,ax_Tf = plt.subplots()
# fig_Tf.set_figwidth(8)
# ax_Tf.errorbar(np.array(detuning)[detuning_working],np.array(Tfinal), yerr = np.array(Tstd), marker = 'o',ls= ' ',label = 'Resultados potencial completo', color = '#4C72B0')
# ax_Tf.plot(delta,T_theory_w_ps,label = 'Teórico pseudopotencial',color = '#55A868')
# ax_Tf.set(yscale = 'log')
# ax_Tf.set_xlabel('detuning (Hz)')
# ax_Tf.set_ylabel('Final temperature (k)')
# ax_Tf.grid(True)
# ax_Tf.legend()
# fig_Tf.savefig('detuning_temp_withtheory.svg')
# fig_Tf.savefig('detuning_temp_withtheory.png')



plt.show()
