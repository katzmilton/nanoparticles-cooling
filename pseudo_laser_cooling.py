from sim_functions import *
import matplotlib.pyplot as plt

name = 'laser_cooling'

long_onda = 1e-6

k = 2*np.pi/long_onda/np.sqrt(3) * np.array([1,1,1])

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 

gamma = 80e6
# detuning = np.arange(1,20,2)*gamma/2
detuning = [(gamma/2)*i for i in np.array([1,5,10,15,20])]
# detuning = [gamma/2]
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha':0, 'pseudo': True}
ions = {'mass': mass, 'charge': charge,'initial_positions': [[2e-5,2e-5,2e-5]]}

for det_i in range(len(detuning)):
    try:
        laser = {'gamma': gamma, 
                 'detuning': detuning[det_i], 
                 # 'emission':False,
                 'k': k} 

        time,positions,velocities = dopplercooling(name,ions,trap,laser,2e8)

        np.savez(f'resultados_pseudo_laser_cooling_detuning_{detuning[det_i]:.2e}_{det_i}.npz',time= time, positions= positions, velocities= velocities,ions = ions,trap=trap) 


        fig,ax= plt.subplots(3,sharex =True)
        ax[0].plot(time,positions[:,0,0])
        ax[1].plot(time,positions[:,0,1])
        ax[2].plot(time,positions[:,0,2])
        ax[2].set_xlabel('Time (s)')
        ax[0].set_ylabel('X (m)')
        ax[1].set_ylabel('Y (m)')
        ax[2].set_ylabel('Z (m)')
        ax[0].grid()
        ax[1].grid()
        ax[2].grid()
        ax[0].set_title(f'resultados_pseudo_laser_cooling_detuning_{detuning[det_i]:.2e}')

        fig.savefig(f'resultados_pseudo_laser_cooling_detuning_{detuning[det_i]:.2e}.png')
    except:
        f = open('fallos.txt','a')
        f.write(f'detuning = {detuning[det_i]:.2e}\n\n')
        f.close()
plt.show()
