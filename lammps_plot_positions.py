import numpy as np
from matplotlib import pyplot as plt

filename = 'lammps_efield.npz'

results = np.load(filename)

fig_pos,ax_pos = plt.subplots(3,sharex =True)
[ax_i.plot(results['time'],results['positions'][:,0,d]) for d,ax_i in enumerate(ax_pos)]
[ax_i.grid() for ax_i in ax_pos]

plt.show()
