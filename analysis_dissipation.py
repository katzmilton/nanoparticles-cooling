import numpy as np
from matplotlib import pyplot as plt
from analysis_functions import *
import scipy.constants as spc

gamma = 0.3e12
# gamma = 80e6
detuning = [gamma/2]
# detuning = np.array([1,5,10,15,20])*gamma/2
for det_i in range(len(detuning)):
    print(det_i)
    print('leyendo resultados')
    # resultados= load_data(f'resultados_pseudo_laser_cooling_detuning_{detuning[det_i]:.2e}_{det_i}.npz')
    # resultados= load_data(f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}.npz')
    resultados= load_data(f'resultados_nanoparticles_detuning_{detuning[det_i]:.2e}.npz')
    ions = resultados['ions']
    energia = energy(resultados['velocities'],resultados['positions'],resultados['trap'],ions)
    indexes,values = envelope(energia)
    print(indexes)
    print(values)
    fig_E, ax_E = plt.subplots()
    ax_E.plot(resultados['time'],energia,label = 'energía')
    ax_E.plot(resultados['time'][indexes],energia[indexes], label = 'envolvente',marker = 'o')
    fig_E.legend()


plt.show()
