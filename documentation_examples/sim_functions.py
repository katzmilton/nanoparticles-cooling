import pylion as pl
import numpy as np
import scipy.constants as spc

def dopplercooling(name,ions,trap,laser,evolve_steps=False):

    s = pl.Simulation(name)

    if trap.get('domain'):
        s.attrs['domain'] = trap['domain']
    
    if ions.get('initial_positions'):
        s.append(pl.placeions(ions,ions['initial_positions']))
    elif ions.get('cloud_radius'):
        try:
            s.append(pl.createioncloud(ions,ions['cloud_radius'][0],ions['cloud_radius'][1]))
        except:
            s.append(pl.createioncloud(ions,ions['cloud_radius'],1))

    else:
        s.append(pl.createioncloud(ions, 1e-4, 1))
    
    ions['uid'] = s[-1]['uid']    

    if type(laser) is list:
        for las_i in range(len(laser)):
            decay_rate = laser[las_i].get('decay_rate',laser[las_i]['gamma'])
            N_ions = ions.get('N_ions',1)
            if laser[las_i].get('T_fin'):
                s.append(pl.langevinbath(laser[las_i]['T_fin'], laser[las_i]['damp'],las_i))
            else:
                if laser[las_i].get('detuning'):
                    ratio = laser[las_i]['detuning']/laser[las_i]['gamma']
                else:
                    ratio = laser[las_i]['ratio']
                sat = laser[las_i].get('saturation',1)
                rho_ee = (sat/2)/(1+sat+(2*ratio)**2)
                if laser[las_i].get('emission',True):
                    s.append(pl.langevinbath(np.linalg.norm(laser[las_i]['k']),laser[las_i]['gamma']*rho_ee,las_i))

            if laser[las_i].get('detuning'):
                if laser[las_i].get('linear'):
                    s.append(pl.linear_photonabsorption(ions,laser[las_i]))
                else:
                    s.append(pl.photonabsorption(ions,laser[las_i]))
    else:
        decay_rate = laser.get('decay_rate',laser['gamma'])
        N_ions = ions.get('N_ions',1)
        if laser.get('T_fin'):
            s.append(pl.langevinbath(laser['T_fin'], laser['damp']))
        else:
            if laser.get('detuning'):
                ratio = laser['detuning']/laser['gamma']
            else:
                ratio = laser['ratio']
            sat = laser.get('saturation',1)
            rho_ee = (sat/2)/(1+sat+(2*ratio)**2)
            if laser.get('emission',True):
                s.append(pl.langevinbath(np.linalg.norm(laser['k']),N_ions*decay_rate*rho_ee))

        if laser.get('detuning'):
            if laser.get('linear'):
                s.append(pl.linear_photonabsorption(ions,laser))
            else:
                s.append(pl.photonabsorption(ions,laser))

    s.append(pl.linearpaultrap(trap,ions,alpha=trap['alpha']))
    timestep = s[-1]['timestep']

    s.append(pl.dump('positions.txt', variables=['x', 'y', 'z'], steps=10))
    s.append(pl.dump('velocities.txt', variables=['vx','vy','vz'], steps = 10))

    if not evolve_steps:
        s.append(pl.evolve(1e3))
    else:
        s.append(pl.evolve(evolve_steps))

    s.execute()
    
    steps, positions = pl.readdump('positions.txt')

    steps, velocities = pl.readdump('velocities.txt')

    time = steps*timestep

    return [time,positions,velocities]


def linear_lasercool_model(name,ions,trap, laser,evolve_steps = False):
    '''
    Simulates laser cooling as a drag force proportional to velocity and a stochastic Langevin force
    '''
    s = pl.Simulation(name)
    
    if ions.get('initial_positions'):
        s.append(pl.placeions(ions,ions['initial_positions']))
    else:
        s.append(pl.createioncloud(ions, 1e-5, 1))
    
    ions['uid'] = s[-1]['uid']
    

    s.append(pl.linearpaultrap(trap,ions,alpha=trap['alpha']))
    timestep = s[-1]['timestep']
    if laser.get('T_fin'):
        print('langevin bath')
        s.append(pl.langevinbath(laser['T_fin'], laser['damp']))

    if laser.get('k_dir'):
        print('beta force')
        k = ions['mass']*1.6e-27/laser['damp']
        kvec = np.array(laser['k_dir'])*k/np.linalg.norm(np.array(laser['k_dir']))
        s.append(pl.lasercool(ions,kvec))


    s.append(pl.dump('positions.txt', variables=['x', 'y', 'z'], steps=1000))
    s.append(pl.dump('velocities.txt', variables=['vx','vy','vz'], steps = 1000))

    if not evolve_steps:
        s.append(pl.evolve(1e4))
    else:
        s.append(pl.evolve(evolve_steps))

    s.execute()


    steps, positions = pl.readdump('positions.txt')
    steps, velocities = pl.readdump('velocities.txt')

    time = steps*timestep
    return [time,positions,velocities]
