import matplotlib.pyplot as plt
from analysis_functions import *
import numpy as np
import scipy.optimize as op

resultados = load_data('resultados_efield_emission_1000particles.npz')
ions = resultados['ions']
trap = resultados['trap']

def modelo(t,A1,w1,phi1):
    return A1*np.cos(w1*t+phi1)

a,q = aq_coefficients(ions,trap)

parametros_iniciales_x = [25,2*np.pi*trap['frequency']*np.sqrt(a[0] + q[0]**2/2)/2,0]
parametros_iniciales_y = [25,2*np.pi*trap['frequency']*np.sqrt(a[1] + q[1]**2/2)/2,0]
parametros_iniciales_z = [25,2*np.pi*trap['frequency']*np.sqrt(a[2] + q[2]**2/2)/2,0]

parametros_optimos_x,cov_x = op.curve_fit(modelo,resultados['time'],resultados['velocities'][:,0,0], p0 = parametros_iniciales_x)
parametros_optimos_y,cov_y = op.curve_fit(modelo,resultados['time'],resultados['velocities'][:,0,1], p0 = parametros_iniciales_y)
parametros_optimos_z,cov_z = op.curve_fit(modelo,resultados['time'],resultados['velocities'][:,0,2], p0 = parametros_iniciales_z)

fig,ax = plt.subplots(3)

ax[0].plot(resultados['time'],resultados['velocities'][:,0,0])
ax[1].plot(resultados['time'],resultados['velocities'][:,0,1])
ax[2].plot(resultados['time'],resultados['velocities'][:,0,2])

ax[0].plot(resultados['time'],modelo(resultados['time'],*parametros_optimos_x))
ax[1].plot(resultados['time'],modelo(resultados['time'],*parametros_optimos_y))
ax[2].plot(resultados['time'],modelo(resultados['time'],*parametros_optimos_z))

print(parametros_optimos_x[1])
print(2*np.pi*trap['frequency']*np.sqrt(a[0] + q[0]**2/2)/2)

plt.show()
