import numpy as np
from analysis_functions import *
import matplotlib.pyplot as plt
from scipy import signal as sg
import ruptures as rpt
import scipy as sp

gamma = 0.3e12
# gamma = 80e6
detuning = [gamma/2]*4
# detuning = np.array([1,2,3])*gamma/2
# detuning = [gamma/2,gamma/2,gamma/2]
# detuning = [i*gamma/2 for i in np.arange(0.2,4,0.2)]
detuning_working = []
cooling_slope = []
slopes_all = []
err_all = []
# mass = ['50','100','200','400','500','2000','4000','5000','50000','5e5']
mass = ['50','500','4000','50000']
fig_treg,ax_treg = plt.subplots(2,2)
nax = [[0,0],[0,1],[1,0],[1,1]]
for det_i in range(len(detuning)):
# try:
# if det_i != 2 and det_i != 4:

            print(det_i)
# resultados = np.load(f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}_{det_i}.npz',allow_pickle = True)
            resultados= load_data(f'resultados_nanoparticles_detuning_{detuning[det_i]:.2e}_{mass[det_i]}.npz')
            ions = resultados['ions']
            macro_freq = min([frequencies(resultados['positions'][:,0,d],resultados['time'][1]-resultados['time'][0])[0] for d in range(3)])
            steps_per_period = int((1/macro_freq)/(resultados['time'][1]-resultados['time'][0]))
            print(steps_per_period)
            temp = measure_temperature(resultados['velocities'],50*steps_per_period,ions['mass'])[0][::1000]
            # resultados_recortados = {} 
            # for kw in resultados:
            #     try:
            #         resultados_recortados[kw] = resultados[kw][::1000]
            #     except:
            #         print(kw)
            # np.savez(f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}_recortados.npz',**resultados_recortados)
            model = 'l2'
            print(f"{(resultados['time'][1]-resultados['time'][0])/100=}")
            print(f"{resultados['positions'][0,0,:]=}")
            print('empiezo binseg')
            resultados['time'] = resultados['time'][::1000]
            algo = rpt.Binseg(model=model).fit(np.diff(temp)/np.diff(resultados['time'][:len(temp)]))
            result = algo.predict(n_bkps=1)
            print('termino binseg')

            print(1,len(result))
            result = [res for res in result]
            result.insert(0,0)

            ax_treg[nax[det_i][0],nax[det_i][1]].plot(resultados['time'][30:len(temp)],temp[30:],zorder=1,label='Temperatura de la simulación')

            slopes_list = []
            err_list = []

            reg_num = np.arange(len(result))
            for i in range(2,len(result)):
                t_i = resultados['time'][result[i-1]:result[i]]
                T_i = temp[result[i-1]:result[i]]
                slope, intercept, _, _, std_err = sp.stats.linregress(t_i,T_i)
                slopes_list.append(slope/temp[result[1]])
                err_list.append(std_err/temp[result[1]])

                ax_treg[nax[det_i][0],nax[det_i][1]].plot(resultados['time'][result[i-1]:result[i]],slope*resultados['time'][result[i-1]:result[i]] + intercept,lw = 3,zorder = 2, label='Ajuste lineal')
# ax_treg.text(resultados['time'][result[i-1]],70-10*i,str(reg_num[i-1]),size = 22)

            cooling_slope.append(slopes_list[-1])
            slopes_all.append(slopes_list)
            err_all.append(err_list)
            detuning_working.append(detuning[det_i])

            f = open('datos.txt','a')
            f.write(f'Simulación {det_i} con detuning = {detuning[det_i]:.2e}\n')
            f.write(f"temperaturas de cambio \t {temp[result]}\n")
            f.write(f'Pendientes \t {slopes_list}\n')
            f.write(f"T final \t {np.mean(temp[result[-2]+100:result[-1]])}\n\n")
            

            color = ['b','r']*len(result)
# for i in range(1,len(result)):
# ax_treg.fill_between([resultados['time'][result[i-1]],resultados['time'][result[i]]],[0,0],[max(temp),max(temp)],[min(temp),min(temp)],color = color[i],alpha = 0.3,interpolate = True)
            ax_treg[nax[det_i][0],nax[det_i][1]].set(yscale = 'log')
            ax_treg[nax[det_i][0],nax[det_i][1]].set_xlabel('Tiempo (s)')
            ax_treg[nax[det_i][0],nax[det_i][1]].set_ylabel('Temperatura (k)')
            ax_treg[nax[det_i][0],nax[det_i][1]].set_title(f'Masa {mass[det_i]} Da')
# ax_treg.set_title(f'{detuning[det_i]}')
ax_treg[0,1].legend()
# fig_treg.tight_layout()
fig_treg.savefig(f'temperature_regions_{mass[det_i]}.pdf')


# except:
# print(f'failed {detuning[det_i]}')

# mass = np.array([50,100,200,400,500,2000,4000,5000,5e4,5e5])
# cont_mass = np.logspace(1,6,100)
# fig_slope,ax_slope = plt.subplots()
# opt,cov = inverse_fit(mass,cooling_slope,[-80,1,0])
# f.write(f"{opt=}\t{cov=}")
# print(opt)
# ax_slope.plot(mass,cooling_slope,'o',label = 'pendientes obtenidas')
# ax_slope.plot(cont_mass,opt[0]/cont_mass**opt[1]+opt[2])
# ax_slope.set(xscale = 'log')
# ax_slope.set_xlabel('Mass (Da)')
# ax_slope.set_ylabel('Temperature slope (k/s)')
# ax_slope.legend()
# fig_slope.savefig('slope_mass.png')
np.savez('slopes.npz',slopes = slopes_all, std_err = err_all, mass = mass)


plt.show()
