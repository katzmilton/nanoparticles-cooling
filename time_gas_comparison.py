from sim_functions import *
import matplotlib.pyplot as plt

name = 'laser_cooling'

T_final = 1e-1
damp = 1e-3

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 

N_particles = 1000
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 300, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha':0, 'pseudo': True}
ions = {'mass': mass, 'charge': charge}

v = np.zeros([1001,N_particles,3])
fallos = []
for i in range(N_particles):
    try:
        laser = {'damp':damp, 'T_fin': T_final}

        time,positions,velocities = linear_lasercool_model(name,ions,trap,laser,1e6)

        v[:,i,:] = velocities[:,0,:]
        
    except:
        print(f'FALLO {i}')
        fallos.append(i)
v = np.delete(v,fallos,axis = 1)   
np.savez(f'resultados_efield_emission_1000particles.npz',time= time, velocities= v,ions = ions,trap = trap) 
