import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl
mpl.rcParams['axes.grid'] = True

resultados = np.load('laser_cooling_python_det_4.00e+07.npz',allow_pickle = True)
simulation = resultados['simulation'].item()

print(simulation['timestep']*simulation['steps'])
print(int(simulation['steps']/simulation.get('save_every',1)))
t = np.linspace(0,simulation['timestep']*simulation['steps'],len(resultados['x']))

fig_pos,[ax_x,ax_y,ax_z] = plt.subplots(3,sharex = True)
ax_x.plot(t,x)
ax_y.plot(t,y)
ax_z.plot(t,z)

plt.show()
