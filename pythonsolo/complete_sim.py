from functions import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['axes.grid']=True

gamma = 80e6
wavelength = 1e-6

trap = {'radius': 3.75e-3, 
        'length': 2.75e-3, 
        'kappa': 0.244,
        'frequency': 3.85e6, 
        'voltage': 500, 
        'endcapvoltage': 15,
        'anisotropy' : 0.9, 
        'alpha': np.pi/8}

ions = {'mass': 40*1.66e-27,
        'charge': 1*1.6e-19,
        'initial_positions': np.array([1e-4*(np.sin(trap['alpha'])+np.cos(trap['alpha'])),1e-4,1e-4*(np.sin(trap['alpha'])-np.cos(trap['alpha']))])}

laser ={'gamma': gamma, 
        'k': np.array([1,1,1])*2*np.pi/(wavelength*np.sqrt(3)),
        'detuning': gamma/2}

simulation = {  'timestep': 1 / np.max(trap['frequency']) / 20000, 
                'steps': int(5e8),
                # 'repetitions': 5,
                # 'variable': 'length',
                'ions': ions,
                'laser': laser,
                'trap': trap}

x,y,z,vx,vy,vz = run_simulation(simulation)

t = np.linspace(0,simulation['timestep']*simulation['steps'],simulation['steps'])

np.savez(f"laser_cooling_python_det_{laser['detuning']:.2e}.npz",t = t[::1000],y = y[::1000],z = z[::1000],vx = vx[::1000],vy = vy[::1000],vz = vz[::1000],simulation = simulation)

fig_pos,[ax_x,ax_y,ax_z] = plt.subplots(3,sharex=True)
ax_x.plot(t[::1000],x[::1000])
ax_y.plot(t[::1000],y[::1000])
ax_z.plot(t[::1000],z[::1000])

fig_pos.savefig(f"laser_cooling_python_det_{laser['detuning']:.2e}.png")


