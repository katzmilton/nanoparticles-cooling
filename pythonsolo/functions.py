import numpy as np
from scipy import signal as sg
import scipy.constants as spc

def photon_emission(simulation):
    laser = simulation['laser']
    dt = simulation['timestep']
    p = 1 - np.exp(-dt*laser['gamma'])
    
    if simulation.get('variable'):
        if simulation['variable'] in simulation.keys():
            reps = len(simulation[simulation['variable']])
        else:
            for val in simulation.values():
                if isinstance(val,dict):
                    if simulation['variable'] in val.keys():
                        reps = len(val[simulation['variable']])
                        break
    else:
        reps = simulation.get('repetitions',1)
    
    def photon_emission_force(t,x,y,z,vx,vy,vz):
        a0 = np.random.rand(reps)
        theta = np.pi*np.random.rand(reps)
        phi = 2*np.pi*np.random.rand(reps)

        heavyside = (a0 < p).astype(int)
        
        fx = spc.hbar*np.linalg.norm(laser['k'],axis = 0)*heavyside/(dt*np.sqrt(3))*2*np.sin(theta)*np.cos(phi)
        fy = spc.hbar*np.linalg.norm(laser['k'],axis = 0)*heavyside/(dt*np.sqrt(3))*2*np.sin(theta)*np.sin(phi)
        fz = spc.hbar*np.linalg.norm(laser['k'],axis = 0)*heavyside/(dt*np.sqrt(3))*np.sqrt(2)*np.cos(theta)
        return [fx.astype('float64'),fy.astype('float64'),fz.astype('float64')]

    return photon_emission_force

def rftrap(simulation):
    trap = simulation['trap']
    charge = simulation['ions']['charge']
    ev = trap['endcapvoltage']
    radius = trap['radius']
    length = trap['length']
    kappa = trap['kappa']
    anisotropy = trap.get('anisotropy', 1)
    offset = trap.get('offset', (0, 0))
    alpha = trap.get('alpha',0)

    voltages = []
    freqs = []

    if simulation.get('variable') == 'voltage':
        trap['voltages'] = trap['voltages'].T
    elif simulation.get('variable') == 'frequency':
        trap['frequency'] = trap['frequency'].T

    if hasattr(trap['voltage'], '__iter__'):
        voltages.extend(trap['voltage'])
        freqs.extend(trap['frequency'])
    else:
        voltages.append(trap['voltage'])
        freqs.append(trap['frequency'])
    
    def rftrap_force(t,x,y,z,vx,vy,vz):
        E_x = 0
        E_y = 0
        E_z = 0
        for i,(v,f) in enumerate(zip(voltages,freqs)):
            
            phase = 2*np.pi*f*t

            x = (x - offset[0])*np.cos(alpha) - z*np.sin(alpha)
            z = z*np.cos(alpha) + (x-offset[0])*np.sin(alpha)
            if hasattr(alpha,'__iter__'):
                y = (y - offset[1])*np.ones(len(alpha))
            else:
                y = (y - offset[1])

            E_x += v*np.cos(phase)*x/radius**2 + (kappa*ev/length**2)*x
            E_y += -v*anisotropy*np.cos(phase)*y/radius**2 + (kappa*ev/length**2)*y
            E_z += v*(anisotropy-1)*np.cos(phase)*z/radius**2 - 2*(kappa*ev/length**2)*z

            
        return [charge*(E_x*np.cos(alpha) + E_z*np.sin(alpha)),charge*E_y,charge*(E_z*np.cos(alpha) - E_x*np.sin(alpha))]

    return rftrap_force

def photon_absorption(simulation):
    ions = simulation['ions']
    laser = simulation['laser']
    gamma = laser['gamma']
    detuning = laser['detuning']

    if simulation.get('variable') == 'k':
        normk = np.linalg.norm(k,axis = 0)
        normk.shape = (len(normk),-1)
        dx,dy,dz = (np.array(laser['k'])/normk).T
        k = np.linalg.norm(laser['k'],axis = 0)
    else:
        dx,dy,dz = np.array(laser['k'])/np.linalg.norm(laser['k'])
        k = np.linalg.norm(laser['k'])

    s = laser.get('saturation',1)

    if laser.get('linear'):
        def photon_absorption_force(t,x,y,z,vx,vy,vz):

            vk = k * (vx * dx + vy * dy + vz * dz) 
            f0 = spc.hbar*k*gamma*(s/2)/((2*detuning/gamma)**2 + 1 + s)
            kappa = 8*vk*detuning/(gamma**2*(1+s+(2*detuning/gamma)**2))

            fx = f0*dx*(1+kappa)
            fy = f0*dy*(1+kappa)
            fz = f0*dz*(1+kappa)
            return [fx.astype('float64'),fy.astype('float64'),fz.astype('float64')]
    else:
        def photon_absorption_force(t,x,y,z,vx,vy,vz):
            vk = k * (vx * dx + vy * dy + vz * dz) 
            fx = spc.hbar*k*gamma*(s/2)*dx/((2*(detuning + vk)/gamma)**2 + 1 + s)
            fy = spc.hbar*k*gamma*(s/2)*dy/((2*(detuning + vk)/gamma)**2 + 1 + s)
            fz = spc.hbar*k*gamma*(s/2)*dz/((2*(detuning + vk)/gamma)**2 + 1 + s)
            return [fx.astype('float64'),fy.astype('float64'),fz.astype('float64')]

    return photon_absorption_force

def initial_conditions(simulation):
    ions = simulation['ions']
    save_every = simulation.get('save_every',1)
    
    if simulation.get('variable'):
        if simulation['variable'] in simulation.keys():
            reps = len(simulation[simulation['variable']])
        else:
            for val in simulation.values():
                if isinstance(val,dict):
                    if simulation['variable'] in val.keys():
                        reps = len(val[simulation['variable']])
                        break

    else:
        reps = simulation.get('repetitions',1)

    if reps == 1:
        x,y,z = [np.zeros(int(simulation['steps']/save_every)).astype('float64') for i in range(3)]
        vx,vy,vz = [np.zeros(int(simulation['steps']/save_every)).astype('float64') for i in range(3)]
        if ions.get('initial_positions').any():
            x[0],y[0],z[0] = ions['initial_positions']

        if ions.get('initial_velocities'):
            vx[0],vy[0],vz[0] = ions['initial_velocities']
    else:
        x,y,z = [np.zeros(int(simulation['steps']/save_every),reps) for i in range(3)]
        vx,vy,vz = [np.zeros(int(simulation['steps']/save_every),reps) for i in range(3)]

        if ions.get('initial_positions').any():
            for r in range(reps):
                if hasattr(ions['initial_positions'][0],'__iter__'):
                    x[0,r],y[0,r],z[0,r] = ions['initial_positions'][r]
                else:
                    x[0,r],y[0,r],z[0,r] = ions['initial_positions']


        if ions.get('initial_velocities'):
            for r in range(reps):
                if hasattr(ions['initial_velocities'],'__iter__'):
                    vx[0,r],vy[0,r],vz[0,r] = ions['initial_velocities'][r]
                else:
                    vx[0,r],vy[0,r],vz[0,r] = ions['initial_velocities']

    
    return [x,y,z,vx,vy,vz]

def run_simulation(simulation):
    ions = simulation['ions']
    dt = simulation['timestep']
    save_every = simulation.get('save_every',1)
    pos_x,pos_y,pos_z,vel_x,vel_y,vel_z = initial_conditions(simulation)

    forces = []
    
    if simulation.get('laser'):
        if 'detuning' in simulation['laser'].keys():
            forces.append(photon_absorption(simulation))
        if simulation['laser'].get('emission',True):
            forces.append(photon_emission(simulation))

    if simulation.get('trap'):
        forces.append(rftrap(simulation))
    
    if simulation.get('add_forces'):
        for f in simulation['add_forces']:
            forces.append(f)

    x,y,z = pos_x[0],pos_y[0],pos_z[0]
    vx,vy,vz = vel_x[0],vel_y[0],vel_z[0]
    t = 0

    accel_x = 0
    accel_y = 0
    accel_z = 0

    for f in forces:
        fx,fy,fz = f(t,x,y,z,vx,vy,vz)
        accel_x += fx/ions['mass']
        accel_y += fy/ions['mass']
        accel_z += fz/ions['mass']

    for i in range(1,simulation['steps']):
        t = i*dt

        vx += 0.5*dt*accel_x
        vy += 0.5*dt*accel_y
        vz += 0.5*dt*accel_z

        x += vx*dt + 0.5*dt**2*accel_x
        y += vy*dt + 0.5*dt**2*accel_y
        z += vz*dt + 0.5*dt**2*accel_z

        accel_x = 0
        accel_y = 0
        accel_z = 0

        for f in forces:
            fx,fy,fz = f(t,x,y,z,vx,vy,vz)
            accel_x += fx/ions['mass']
            accel_y += fy/ions['mass']
            accel_z += fz/ions['mass']

        vx += 0.5*dt*accel_x
        vy += 0.5*dt*accel_y
        vz += 0.5*dt*accel_z
    
        if i%save_every == 0:

            vel_x[int(i/save_every)] = vx
            vel_y[int(i/save_every)] = vy
            vel_z[int(i/save_every)] = vz

            pos_x[int(i/save_every)] = x
            pos_y[int(i/save_every)] = y
            pos_z[int(i/save_every)] = z

            print(i)

    return [pos_x.T,pos_y.T,pos_z.T,vel_x.T,vel_y.T,vel_z.T] 

def frequencies(signal,delta_time):
    '''
    returns an array with the main frequencies of a real signal ordered by intensity descending
    '''

    if len(signal.shape) == 1:
        fft_signal = np.abs(np.fft.rfft(signal,axis=0))
        freqs = np.fft.rfftfreq(len(signal),delta_time)
        indexes,_ = sg.find_peaks(fft_signal,height = np.max(fft_signal)/10)
        ord_freq = freqs[indexes[fft_signal[indexes].argsort()]][::-1]
    else:
        ord_freq = []
        for rep in range(len(signal)):
            fft_signal = np.abs(np.fft.rfft(signal[rep]))
            freqs = np.fft.rfftfreq(len(signal[rep]),delta_time)
            indexes,_ = sg.find_peaks(fft_signal,height = np.max(fft_signal)/500)
            ord_freq.append(freqs[indexes[fft_signal[indexes].argsort()]][::-1])

    return ord_freq

def measure_temperature(velocities,nsteps,mass =1):
    print(f'{len(velocities)=}')
    vel2 = np.array([(velocities[d])**2 for d in range(3)])

    product = mass/6/spc.k

    temp = np.sum([product*runningMean_fft(vel2[d],nsteps) for d in range(3)],axis = 0)

    print(f'{len(temp)=}')

    return np.array(temp)

def runningMean_fft(x,N):
    x_fft = np.fft.rfft(x)
    window_fft = np.fft.rfft(np.ones(N)/N,len(x))
    return np.fft.irfft(x_fft*window_fft).real
