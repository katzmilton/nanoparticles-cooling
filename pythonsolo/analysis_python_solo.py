from functions import *
import numpy as np
from matplotlib import pyplot as plt

gamma = 80e6
detuning = np.array([1])*gamma/2

Tfinal = []
Tstd = []
detuning_working = []

for det_i in range(len(detuning)):
    print(det_i)
    print('leyendo resultados')
    resultados = dict(np.load(f'python_results_damping_dt_1.30e-11_steps_1e+09.npz',allow_pickle = True))
    simulation = resultados['simulation'].item()
    ions = simulation['ions']
    time = resultados['t']
    dt = time[1]-time[0]
    # steps = int(simulation['steps']/1000)
    # time = np.linspace(0,steps*dt,steps)



    macro_freq = simulation['trap']['frequency']
    steps_per_period = int((1/macro_freq)/(dt))
    print(steps_per_period)

    temp = measure_temperature([resultados['vx'],resultados['vy'],resultados['vz']],50*steps_per_period,ions['mass'])
    resultados.update(temperature = temp)
    print('guardando temperatura')
    np.savez(f'laser_cooling_python_det_{detuning[det_i]:.2e}.npz',**resultados)
     

    Tfinal.append(np.mean(temp[-10000*steps_per_period:]))
    Tstd.append(np.std(temp[-10000*steps_per_period:],ddof=1)/np.sqrt(len(temp[-10000*steps_per_period:])))
    detuning_working.append(det_i)

    print('graficando temperatura')
    fig_T,ax_T = plt.subplots()

    ax_T.semilogy(time,temp)
    ax_T.semilogy(time[-10000*steps_per_period:],Tfinal[-1]*np.ones(10000*steps_per_period))
    ax_T.set_xlabel('Time (s)')
    ax_T.set_ylabel('Temperature (k)')
    ax_T.grid()
    fig_T.savefig(f'temperatura_detunning_{detuning[det_i]:.2e}_{det_i}.png')
            
##
delta = np.linspace(0.2,20.2,100)*gamma/2
saturation = 1

# T_theory_s1 = spc.hbar*gamma**2*(1+(delta/gamma)**2)/(6*spc.k*delta)
# T_theory_s2 = spc.hbar*gamma**2*(1+(delta/gamma)**2)**2/(6*spc.k*delta)
T_theory_w = spc.hbar*gamma**2*(1+saturation+(2*delta/gamma)**2)**2/(48*saturation*delta*spc.k)
# T_final_given = spc.hbar*gamma**2*(1+(delta/gamma)**2)**2/(2*spc.k*delta)

fig_Tf,ax_Tf = plt.subplots()
# fig_Tf.set_figwidth(8)
print(detuning)
print(detuning_working)
print(Tfinal)
ax_Tf.errorbar(np.array(detuning)[detuning_working],np.array(Tfinal), yerr = np.array(Tstd), marker = 'o',ls= ' ',label = 'resultados')
# ax_Tf.plot(delta,T_theory_s1,label = 'predicción teórica Stenholm')
# ax_Tf.plot(delta,T_theory_s2,label = 'predicción teórica Stenholm cuadrado')
ax_Tf.plot(delta,T_theory_w,label = 'predicción teórica wineland')
# ax_Tf.plot(delta,T_final_given,label = 'por construcción')
# ax_Tf.plot(delta,T_theory_w,label = 'predicción teórica wineland')
ax_Tf.set(yscale = 'log')
# ax_Tf.set_title('D calculado con $\Delta = \Gamma/\sqrt{3}$ e $I = 1$, T multiplicado por 12')
ax_Tf.set_xlabel('detuning (Hz)')
ax_Tf.set_ylabel('Final temperature (k)')
ax_Tf.grid()
ax_Tf.legend()
fig_Tf.savefig('detuning_temp_withtheory.svg')
fig_Tf.savefig('detuning_temp_withtheory.jpeg')

plt.show()
