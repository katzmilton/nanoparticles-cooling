from myfuncs import *
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as spc

name = Path(__file__).stem 

damp = [1e22]
for i in range(8):
    damp.append(round(damp[-1]*10))
damp = np.array(damp)

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 
x0 = [[1e-4,1e-4,1e-4]]

ions = {'mass': mass, 'charge': charge, 'initial_positions': x0} 
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9,'alpha': np.pi/8}

Tavg_final = []

fig,ax = plt.subplots(1,sharex = True)
for d_i in range(len(damp)):
    try:

        laser = {'damp':damp[d_i],'k_dir':[1,1,1]}
        time,positions,velocities = linear_lasercool_model(name,ions,trap,laser,1e6)
        np.savez(f'resultados_damp_{damp[d_i]:.0e}',time=time,positions=positions,velocities=velocities,ions = ions)


        

    except:

        f = open(f"errores.txt", "a") 
        f.write(f'Simulation with damp = {damp[d_i]} not ralized\n')
        f.close
##

