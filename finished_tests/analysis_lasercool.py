import numpy as np
import time as tm
from myfuncs import *
from matplotlib import pyplot as plt

#Esto tiene que estar como en el otro código, no se me ocurre una manera mejor de hacerlo
damp = [1e27]
for i in range(3):
    damp.append(round(damp[-1]*10))
damp = np.array(damp)


Tavg_final = []
Tstd_final = []


f = open('datos.txt','a')

fig_T,ax_T = plt.subplots()


for d_i in range(len(damp)):
    f.write(f'\nSimulation with damp = {damp[d_i]:.0e}\n')
    resultados = np.load(f'resultados_damp_{damp[d_i]:.0e}.npz',allow_pickle=True)
    time = resultados['time']
    positions = resultados['positions']
    velocities = resultados['velocities']
    ions = resultados.get('ions',np.array({'mass': 40})).item()

    macro_freq = min([frequencies(positions[:,0,d],time[1]-time[0])[0] for d in range(3)])
    f.write(f'macro motion frequency = {macro_freq:e} Hz\n')

    steps_per_period = int((1/macro_freq)/(time[1]-time[0]))

    
    print(f'calculando damp = {damp[d_i]:.0e}')
    T_porlammps = measure_temperature(velocities,10*steps_per_period,ions['mass'])[0][:-10*steps_per_period] 
    Tavg_final.append(np.mean(T_porlammps[-10*steps_per_period:]))
    Tstd_final.append(np.std(T_porlammps[-10*steps_per_period:],ddof=1))
    f.write(f'Final temperature= ({Tavg_final[-1]} +- {Tstd_final[-1]}) k\n')


    ax_T.semilogy(time[:len(T_porlammps)],T_porlammps,label = f'damp = {damp[d_i]:.0e}')
    ax_T.set_ylabel('Temperature (k)')
    ax_T.set_xlabel('Time (s)')

    figx,axx = plt.subplots(3,sharex =True)
    axx[0].plot(time,positions[:,0,0])
    axx[1].plot(time,positions[:,0,1])
    axx[2].plot(time,positions[:,0,2])
    axx[2].set_xlabel('Time (s)')
    axx[0].set_ylabel('X (m)')
    axx[1].set_ylabel('Y (m)')
    axx[2].set_ylabel('Z (m)')
    axx[0].grid()
    axx[1].grid()
    axx[2].grid()
    figx.savefig(f'Positions_damp_{damp[d_i]}.jpeg')

fig_Tf,ax_Tf = plt.subplots()
ax_Tf.loglog(np.array(damp),np.array(Tavg_final),'o')
ax_Tf.set_xlabel('damp ($s^{-1}$)')
ax_Tf.set_ylabel('Calculated final T (k)')
ax_Tf.grid()
fig_Tf.savefig('calibrate_T.jpeg')

ax_T.grid()
fig_T.legend()
fig_T.savefig('Temperature.jpeg')

f.close()
