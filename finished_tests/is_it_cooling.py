import pylion as pl
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as spc
from myfuncs import *

name = Path(__file__).stem 

k_d = [[1,0,0]]#[[1,1,1],[1,0,1],[0,0,1],[1,0,0]]
text = ['Cooling all directions','not cooling in the y eigendirection','cooling only in z direction','cooling only in x direction']
damp_Tfin = np.array([[1e-4,1e-10],[1e-4,1e-11],[1e-5,1e-9],[1e-5,1e-10],[1e-5,1e-11],[1e-6,1e-8],[1e-6,1e-9],[1e-6,1e-11],[1e-6,1e-8]])#np.array([[1e-2,1e-9],[1e-3,1e-7],[1e-3,1e-8],[1e-3,1e-9],[1e-3,1e-10],[1e-3,1e-11],[1e-4,1e-6],[1e-4,1e-7],[1e-4,1e-9],[1e-4,1e-10],[1e-4,1e-11],[1e-5,1e-9],[1e-5,1e-10],[1e-5,1e-11],[1e-6,1e-8],[1e-6,1e-9],[1e-6,1e-11],[1e-6,1e-8]])
# damp = np.logspace(-6,0,7)
damp = damp_Tfin[:,0]
Tfin = damp_Tfin[:,1]
# Tfin = np.logspace(-11,-1,11)
for ki in range(len(k_d)):
    for Ti in range(len(Tfin)):
        try:
            mass = 40 #Da
            charge = 1 #e

            trap_radius = 3.75e-3
            trap_length = 2.75e-3
            trap_kappa  = 0.244
            trap_freq = 3.85e6 

            ions = {'mass': mass, 'charge': charge} 
            trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
                    'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha': np.pi/8,}
            laser = {'damp': damp[Ti],'k_dir': k_d[ki], 'T_fin': Tfin[Ti]}

            time,positions,velocities = linear_lasercool_model(name,ions,trap,laser,1e6)


            #Obtengo la frecuencia de macromoción con fft
            fft_positions = np.fft.rfft(positions[:,0,:],axis = 0)
            ion_freq = np.fft.rfftfreq(positions.shape[0],max(time)/len(time))
            max_frequency_index_x = int(np.where(abs(fft_positions[:,0]) == max(abs(fft_positions[:,0])))[0])
            max_frequency_index_y = int(np.where(abs(fft_positions[:,1]) == max(abs(fft_positions[:,1])))[0])
            max_frequency_index_z = int(np.where(abs(fft_positions[:,2]) == max(abs(fft_positions[:,2])))[0])
            ##
            macro_freq = np.array([ion_freq[max_frequency_index_x],ion_freq[max_frequency_index_y],ion_freq[max_frequency_index_z]])


            steps_per_period = ((1/macro_freq)/(time[1]-time[0])).astype(int)
            
            T_porlammps = measure_temperature(velocities,50*max(steps_per_period),mass)[0] 

            s = mass*1.6e-27*action(positions,velocities,50*max(steps_per_period))[0]/spc.hbar


            fig,ax = plt.subplots(2,sharex = True)
            ax[1].semilogy(time,s,label = 'action')
            ax[0].semilogy(time,T_porlammps,label = 'Temperature')
            ax[1].set_xlabel('Time (s)')
            ax[0].set_ylabel('Temperature (k)')
            ax[1].set_ylabel('Action / $\hbar$')
            ax[0].legend()
            ax[1].legend()
            ax[0].grid()
            ax[1].grid()

            fig.savefig(f'Temperature_{k_d[ki]}_{damp[Ti]}_{Tfin[Ti]}.jpeg')

            # # ax.plot(data[:, 0, 0], data[:, 0, 1], data[:, 0, 2])
            # # ax.set_xlabel('x (um)')
            # # ax.set_ylabel('y (um)')
            # # ax.set_zlabel('z (um)')


            figx,axx = plt.subplots(3,sharex =True)
            axx[0].plot(time,positions[:,0,0])
            axx[1].plot(time,positions[:,0,1])
            axx[2].plot(time,positions[:,0,2])
            axx[2].set_xlabel('Time (s)')
            axx[0].set_ylabel('X (m)')
            axx[1].set_ylabel('Y (m)')
            axx[2].set_ylabel('Z (m)')
            axx[0].grid()
            axx[1].grid()
            axx[2].grid()


            figx.savefig(f'Positions_{k_d[ki]}_{damp[Ti]}_{Tfin[Ti]}.jpeg')
            
            f = open(f"datos.txt", "a") 
            f.write(text[ki])
            f.write(f"\nMacromotion frequency: \t {macro_freq} Hz\n")
            f.write(f"Final temperature: \t {T_porlammps[-max(steps_per_period)]} k\n")
            f.write(f"Final action: \t {s[0]}\n\n")
            f.close
            np.savez(f'resultados_{k_d[ki]}_{damp[Ti]}_{Tfin[Ti]}.npz',time = time,positions = positions,k_d = k_d,damp=damp,velocities = velocities)
        except:
            f = open('datos.txt','a')
            f.write(f'\n{k_d[ki]}_{damp[Ti]}_{Tfin[Ti]} did not work\n')
            f.close
