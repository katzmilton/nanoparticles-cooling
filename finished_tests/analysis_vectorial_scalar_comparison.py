##
import numpy as np
from myfuncs import *
from matplotlib import pyplot as plt
import scipy.constants as spc

gamma = 80e6
mass = 40
long_onda = 1e-6
k = [2*np.pi/long_onda/np.sqrt(3)]*3
# detuning = [i*gamma/2 for i in np.arange(1,5,1)]
detuning = np.array([1,5,10,20])*gamma/2

T_final = spc.hbar*gamma**2*(1+(detuning/gamma)**2)**2/(2*spc.k*detuning)
damp = mass*1.66e-27*(1+(detuning/gamma)**2)**2*gamma/(2*spc.hbar*np.linalg.norm(k)**2*detuning)

# detuning = [gamma/2]*4
# T_final = np.logspace(-4,-1,4)
# damp = np.logspace(-6,-4,1)

Tfinal_original = []
Tstd_original = []
Tfinal_beta = []
Tstd_beta = []

detuning_working = []
for det_i in range(len(detuning)):
    try:
        print(det_i)
        print('leyendo resultados')
        # resultados_original = dict(np.load(f'resultados_detuning_{detuning[det_i]:.2e}_originallammps.npz',allow_pickle = True))
        resultados_original = dict(np.load(f'resultados_damp_{damp[det_i]}_Tfinal_{T_final[det_i]}.npz',allow_pickle = True))
        # resultados_beta = dict(np.load(f'resultados_detuning_{detuning[det_i]:.2e}_beta.npz',allow_pickle = True))
        resultados_beta = dict(np.load(f'resultados_damp_{damp[det_i]:.2e}_Tfinal_{T_final[det_i]:.2e}_beta.npz',allow_pickle = True))
        ions = resultados_original.get('ions',np.array({'mass': 40})).item() #los iones son los mismos en los dos
        macro_freq_original = min([frequencies(resultados_original['positions'][:,0,d],resultados_original['time'][1]-resultados_original['time'][0])[0] for d in range(3)])
        macro_freq_beta = min([frequencies(resultados_beta['positions'][:,0,d],resultados_beta['time'][1]-resultados_beta['time'][0])[0] for d in range(3)])

        steps_per_period_original = int((1/macro_freq_original)/(resultados_original['time'][1]-resultados_original['time'][0]))
        steps_per_period_beta= int((1/macro_freq_beta)/(resultados_beta['time'][1]-resultados_beta['time'][0]))

        if (steps_per_period_original < 700) and (steps_per_period_beta < 700):
            # print('calculando temperatura')
            # temp = resultados['temperature']
            temp_original = measure_temperature(resultados_original['velocities'],50*steps_per_period_original,ions['mass'])[0]
            temp_beta = measure_temperature(resultados_beta['velocities'],50*steps_per_period_beta,ions['mass'])[0]
            resultados_original.update(temperature = temp_original)
            resultados_beta.update(temperature = temp_beta)
            print('guardando temperatura')
            # np.savez(f'resultados_detuning_{detuning[det_i]:.2e}_originallammps.npz',**resultados_original)
            # np.savez(f'resultados_detuning_{detuning[det_i]:.2e}_beta.npz',**resultados_beta)

            Tfinal_original.append(np.mean(temp_original[-5000*steps_per_period_original:]))
            Tstd_original.append(np.std(temp_original[-5000*steps_per_period_original:],ddof=1))
            Tfinal_beta.append(np.mean(temp_beta[-5000*steps_per_period_beta:]))
            Tstd_beta.append(np.std(temp_beta[-5000*steps_per_period_beta:],ddof=1))
            detuning_working.append(det_i)

            print('graficando temperatura')
            fig_T,ax_T = plt.subplots()
            ax_T.semilogy(resultados_original['time'][:len(temp_original)],temp_original,label = 'lammps original')
            ax_T.semilogy(resultados_original['time'][-5000*steps_per_period_original:],Tfinal_original[-1]*np.ones(5000*steps_per_period_original))
            ax_T.semilogy(resultados_beta['time'][:len(temp_beta)],temp_beta,label = 'beta model')
            ax_T.semilogy(resultados_beta['time'][-5000*steps_per_period_beta:],Tfinal_beta[-1]*np.ones(5000*steps_per_period_beta))
            ax_T.set_xlabel('Time (s)')
            ax_T.set_ylabel('Temperature (k)')
            ax_T.grid()
            ax_T.legend()
            fig_T.savefig(f'temperatura_detunning_{detuning[det_i]:.2e}.png')

    except:
        print(f'failed {detuning[det_i]}')
##
delta = np.linspace(0.2,20,100)*gamma/2

T_final_given = spc.hbar*gamma**2*(1+(delta/gamma)**2)**2/(2*spc.k*delta)

fig_Tf,ax_Tf = plt.subplots()
fig_Tf.set_figwidth(8)
ax_Tf.errorbar(np.array(detuning)[detuning_working],np.array(Tfinal_original), yerr = np.array(Tstd_original), marker = 'o',ls= ' ',label = 'resultados lammps original')
ax_Tf.errorbar(np.array(detuning)[detuning_working],np.array(Tfinal_beta), yerr = np.array(Tstd_beta), marker = 'o',ls= ' ',label = 'resultados modelo beta')
ax_Tf.plot(delta,T_final_given,label = 'por construcción')
# ax_Tf.plot(delta,T_theory_w,label = 'predicción teórica wineland')
ax_Tf.set(yscale = 'log')
# ax_Tf.set_title('D calculado con $\Delta = \Gamma/\sqrt{3}$ e $I = 1$, T multiplicado por 12')
ax_Tf.set_xlabel('detuning (Hz)')
ax_Tf.set_ylabel('Final temperature (k)')
ax_Tf.grid()
ax_Tf.legend()
# fig_Tf.savefig('detuning_temp_withtheory.svg')
fig_Tf.savefig('detuning_temp_withtheory.jpeg')

# T_final = spc.hbar*gamma**2*(1+(detuning/gamma)**2)**2/(2*spc.k*detuning)

fig_cal, ax_cal = plt.subplots()
ax_cal.loglog(T_final,Tfinal_original,'o')
x = np.linspace(min(T_final),max(T_final),2)
ax_cal.loglog(x,x)
ax_cal.grid()
# fig_cal.savefig('simulados_como_antes.png')

plt.show()
