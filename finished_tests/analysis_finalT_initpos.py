import numpy as np
from matplotlib import pyplot as plt
from myfuncs import *

gamma = 80e6
# detuning = [(gamma/2)*i for i in np.arange(0,4,0.2)]


cloud_radius = [1e-3,1e-4,1e-5,1e-6,1e-7]

Tfinal = []
Tstd = []
for cloud in range(len(cloud_radius)):
    try:
        print(cloud)
        resultados = np.load(f"resultados_laser_cooling_init_pos_{cloud_radius[cloud]:.2e}.npz", allow_pickle = True)
        positions = resultados['positions']
        velocities = resultados['velocities']
        time = resultados['time']
        ions = resultados.get('ions',np.array({'mass': 40})).item()

        macro_freq = min([frequencies(positions[:,0,d],time[1]-time[0])[0] for d in range(3)])

        steps_per_period = int((1/macro_freq)/(time[1]-time[0]))

        temp = measure_temperature(velocities,steps_per_period) 
        if steps_per_period < 700:
            temp = measure_temperature(velocities,50*steps_per_period,ions['mass'])[0]
             
            fig_T,ax_T = plt.subplots()
            ax_T.semilogy(time[:len(temp)],temp)
            ax_T.set_xlabel('Time (s)')
            ax_T.set_ylabel('Temperature (k)')
            ax_T.grid()
            fig_T.savefig(f'temperatura_initial_pos_{cloud_radius[cloud]:.2e}.png')

            Tfinal.append(np.mean(temp[-100*steps_per_period:]))
            Tstd.append(np.std(temp[-100*steps_per_period:],ddof=1))

    except:
        print(f'failed {detuning[det_i]}')

fig_Tf,ax_Tf = plt.subplots()
fig_Tf.set_figwidth(8)
ax_Tf.errorbar(np.array(cloud_radius),np.array(Tfinal), yerr = np.array(Tstd), marker = 'o',ls= ' ')
ax_Tf.set(yscale = 'log')
ax_Tf.set(xscale = 'log')

ax_Tf.set_xlabel('Initial cloud radius (m)')
ax_Tf.set_ylabel('Final temperature (k)')
ax_Tf.grid()
fig_Tf.savefig('cloud_temp.svg')
fig_Tf.savefig('cloud_temp.jpeg')

plt.show()
