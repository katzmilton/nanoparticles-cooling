import pylion as pl
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as spc
from myfuncs import *

name_dop = 'doppler'
name_lin = 'linear'

long_onda = 1e-6
k_d = [[2*np.pi/long_onda/np.sqrt(3)]*3, [2*np.pi/long_onda/np.sqrt(2),0,2*np.pi/long_onda/np.sqrt(2)],[2*np.pi/long_onda,0,0],[0,0,2*np.pi/long_onda]]


mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 

gamma = 80000
detuning = gamma/2
s = 1
epsilon = spc.hbar*np.linalg.norm(k_d,axis=1)**2/(2*mass*1.6e-27)
damp = (1 + (detuning/gamma)**2)**2 * gamma/(4*s*epsilon*detuning)

max_init_pos = gamma/(4*np.pi*trap_freq*max(np.linalg.norm(k_d,axis = 1))) - np.sqrt(spc.k * 1e-4/(trap_freq*20*mass*1.6e-27*1e-5))/(2*np.pi*trap_freq)

ions = {'mass': mass, 'charge': charge, 'initial_positions': [[max_init_pos/10]*3]} 
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha': np.pi/8}

##
for ki in range(len(k_d)):
    print(f'SImulación {ki}')
    laser = {'gamma': gamma, 'k_dir': k_d[ki],'k': k_d[ki], 'detuning': gamma/2,'damp': damp[ki], 'T_fin': 1e-10}

    time_dop,positions_dop,velocities_dop = dopplercooling(name_dop,ions,trap,laser,2e6)
    time_lin,positions_lin,velocities_lin = linear_lasercool_model(name_lin,ions,trap,laser,1e4)
    np.savez(f'resultados_comparacion_doppler_laser_{k_d[ki]}.npz',time_dop = time_dop, positions_dop = positions_dop, velocities_dop = velocities_dop, time_lin=time_lin, positions_lin = positions_lin, velocities_lin = velocities_lin,ions = ions)
    
    

    fig_dop,ax_dop = plt.subplots(3,sharex =True)
    ax_dop[0].plot(time_dop[::5],positions_dop[::5,0,0])
    ax_dop[1].plot(time_dop[::5],positions_dop[::5,0,1])
    ax_dop[2].plot(time_dop[::5],positions_dop[::5,0,2])
    ax_dop[2].set_xlabel('Time (s)')
    ax_dop[0].set_ylabel('X (m)')
    ax_dop[1].set_ylabel('Y (m)')
    ax_dop[2].set_ylabel('Z (m)')
    ax_dop[0].grid()
    ax_dop[1].grid()
    ax_dop[2].grid()
    ax_dop[0].set_title(f'doppler cooling_{k_d[ki][0]:.1e}')

    fig_lin,ax_lin = plt.subplots(3,sharex =True)
    ax_lin[0].plot(time_lin[::5],positions_lin[::5,0,0])
    ax_lin[1].plot(time_lin[::5],positions_lin[::5,0,1])
    ax_lin[2].plot(time_lin[::5],positions_lin[::5,0,2])
    ax_lin[2].set_xlabel('Time (s)')
    ax_lin[0].set_ylabel('X (m)')
    ax_lin[1].set_ylabel('Y (m)')
    ax_lin[2].set_ylabel('Z (m)')
    ax_lin[0].grid()
    ax_lin[1].grid()
    ax_lin[2].grid()
    ax_lin[0].set_title(f'lasercool_{k_d[ki][0]:.1e}')

    fig_dop.savefig(f'Positions_doppler_{k_d[ki][0]:.1e}.jpeg')
    fig_lin.savefig(f'Positions_lasercool_{k_d[ki][0]:.1e}.jpeg')
