from matplotlib import pyplot as plt
from myfuncs import *

long_onda = 1e-6
k_d = [[2*np.pi/long_onda/np.sqrt(3)]*3, [2*np.pi/long_onda/np.sqrt(2),0,2*np.pi/long_onda/np.sqrt(2)],[2*np.pi/long_onda,0,0],[0,0,2*np.pi/long_onda]]
text = ['Cooling all directions','not cooling in the y eigendirection','cooling only in z direction','cooling only in x direction']

Tavg_final = []
Tstd_final = []

nperiods = 50

f = open('datos.txt','a')

for ki in range(len(k_d)):
    f.write(text[ki])
    print(text[ki])
    resultados = np.load(f'resultados_comparacion_doppler_laser_{k_d[ki]}.npz',allow_pickle = True)

    time_dop = resultados['time_dop']
    positions_dop = resultados['positions_dop']
    velocities_dop = resultados['velocities_dop']

    time_lin = resultados['time_lin']
    positions_lin = resultados['positions_lin']
    velocities_lin = resultados['velocities_lin']

    ions = resultados.get('ions',np.array({'mass': 40})).item()
    try:
        macro_freq_dop = min([frequencies(positions_dop[:,0,d],time_dop[1]-time_dop[0])[0] for d in range(3)])
        f.write(f'macro motion frequency for dopplercooling  \t {macro_freq_dop:e} Hz\n')

        macro_freq_lin = min([frequencies(positions_lin[:,0,d],time_lin[1]-time_lin[0])[0] for d in range(3)])
        f.write(f'macro motion frequency for lasercool  \t {macro_freq_lin:e} Hz\n')

        steps_per_period_dop = int((1/macro_freq_dop)/(time_dop[1]-time_dop[0]))
        steps_per_period_lin = int((1/macro_freq_lin)/(time_lin[1]-time_lin[0]))

        print(steps_per_period_dop,steps_per_period_lin)

        print(f'Calculando temperaturas')
        T_dop = measure_temperature(velocities_dop,nperiods*steps_per_period_dop,ions['mass'])[0][:-nperiods*steps_per_period_dop] 
        T_lin = measure_temperature(velocities_lin,nperiods*steps_per_period_lin,ions['mass'])[0][:-nperiods*steps_per_period_lin] 
                
        print(f'Calculando acción')
        s_dop = ions['mass']*1.6e-27*action(positions_dop,velocities_dop,nperiods*steps_per_period_dop)[0][:-nperiods*steps_per_period_dop]/spc.hbar
        s_lin = ions['mass']*1.6e-27*action(positions_lin,velocities_lin,nperiods*steps_per_period_lin)[0][:-nperiods*steps_per_period_lin]/spc.hbar

        print('graficando s ')
        fig,ax = plt.subplots(2,sharex = True)
        ax[1].semilogy(time_dop[:len(s_dop)],s_dop,label = 'dopplercooling')
        ax[1].semilogy(time_lin[:len(s_lin)],s_lin,label = 'lasercool')
        print('graficando T')
        ax[0].semilogy(time_dop[:len(T_dop)],T_dop,label = 'dopplercooling')
        ax[0].semilogy(time_lin[:len(T_lin)],T_lin,label = 'lasercool')

        print('seteando formato')
        ax[1].set_xlabel('Time (s)')
        ax[0].set_ylabel('Temperature (k)')
        ax[1].set_ylabel('Action / $\hbar$')
        ax[0].legend('upper right')
        ax[1].legend('upper right')
        ax[0].grid()
        ax[1].grid()
        fig.savefig(f'cooling_comparison_{k_d[ki]}.jpeg')
    except:
        print('Analysis failed')
