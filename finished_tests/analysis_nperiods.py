import numpy as np
from myfuncs import *
from matplotlib import pyplot as plt
import time as tm

#Esto tiene que estar como en el otro código, no se me ocurre una manera mejor de hacerlo
nperiods = [1,10,20,30,40,50,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200]
T_final = np.logspace(-7,-4,1)
damp = np.logspace(-6,-4,3)

Tavg_final = [[] for i in range(len(nperiods))]
Tstd_final = [[] for i in range(len(nperiods))]
Tf_working = [[] for i in range(len(nperiods))]
f = open('datos.txt','a')
damp_final = []

##
for T_i in range(len(T_final)):
    for d_i in range(len(damp)):
        f.write(f'\nSimulation with damp = {damp[d_i]:.0e} and final T = {T_final[T_i]:.0e}\n')

        resultados = np.load(f'resultados_damp_{damp[d_i]}_Tfinal_{T_final[T_i]}.npz',allow_pickle = True)
        time = resultados['time']
        positions = resultados['positions']
        velocities = resultados['velocities']
        ions = resultados.get('ions',np.array({'mass': 40})).item()

        macro_freq = min([frequencies(positions[:,0,d],time[1]-time[0])[0] for d in range(3)])
        f.write(f'macro motion frequency  \t {macro_freq:e} Hz\n')
        
        steps_per_period = int((1/macro_freq)/(time[1]-time[0]))

        print(f'\nSimulation with damp = {damp[d_i]:.0e} and final T = {T_final[T_i]:.0e}\n')

        # fig,ax = plt.subplots(2,sharex = True)
        if steps_per_period < 700:
            damp_final.append(damp[d_i])
            for n in range(len(nperiods)):
                f.write(f'Ventana de {nperiods[n]} periodos\n')
                print(f'Ventana de {nperiods[n]} periodos')
                dt = tm.time()
                T_porlammps = measure_temperature(velocities,nperiods[n]*steps_per_period,ions['mass'])[0]
                f.write(f'Tardo {tm.time()-dt}\n')
                # s = ions['mass']*1.6e-27*action(positions,velocities,nperiods[n]*steps_per_period)[0]/spc.hbar

                Tavg_final[n].append(np.mean(T_porlammps[-1000*steps_per_period:]))
                Tstd_final[n].append(np.std(T_porlammps[-1000*steps_per_period:],ddof=1))
                Tf_working[n].append(T_final[T_i])
                f.write(f'Final temperature \t ({Tavg_final[n][-1]} +- {Tstd_final[n][-1]}) k\n')
                # f.write(f"Final action: \t {np.mean(s[-1000*steps_per_period:])}\n")

                # ax[1].semilogy(time[:len(s)],s,label = f'{nperiods[n]} periodos')
                # ax[0].semilogy(time[:len(T_porlammps)],T_porlammps,label = f'{nperiods[n]} periodos')
                # ax[1].set_xlabel('Time (s)')
                # ax[0].set_ylabel('Temperature (k)')
                # ax[1].set_ylabel('Action / $\hbar$')

            # ax[0].legend(loc = 1)
            # ax[1].legend(loc = 1)
            # ax[0].grid()
            # ax[1].grid()
            # fig.savefig(f'Temperature_lanT_{T_final[T_i]:.0e}_damp_{damp[d_i]:.0e}.jpeg',dpi=100)

        
        else:
            f.write(f'{steps_per_period} steps too long to average \n')
##
Tstd_final = np.array(Tstd_final)
Tavg_final = np.array(Tavg_final)
fig_std,ax_std = plt.subplots()

for i in range(len(damp_final)):
    if damp_final[i] == 1e-6:
        ax_std.plot(nperiods,Tstd_final[:,i]/Tavg_final[:,i],marker = 'o',color =  'b', ls = '')
    if damp_final[i] == 1e-5:
        ax_std.plot(nperiods,Tstd_final[:,i]/Tavg_final[:,i],marker = 'o',color =  'orange', ls='')
    if damp_final[i] == 1e-4:
        ax_std.plot(nperiods,Tstd_final[:,i]/Tavg_final[:,i],marker = 'o',color =  'green',ls = '')

ax_std.legend(['damp = 1e-6','damp = 1e-5','damp = 1e-4'])
ax_std.grid()
ax_std.set_xlabel('Window size')
ax_std.set_ylabel('Relative error of T')
fig_std.savefig('sigma_window.jpeg',dpi = 100)
plt.plot()
##
# fig_final,ax_final = plt.subplots()
# for i in range(len(Tf_working)):
#     ax_final.errorbar(np.array(Tf_working[i]),np.array(Tavg_final[i]),yerr = np.array(Tstd_final[i]),ls = '',marker = 'o',label = f'{nperiods[i]} periodos')

# ax_final.set(xscale = 'log')
# ax_final.set(yscale = 'log')
# ax_final.set_xlabel('Langevin final temperature (k)')
# ax_final.set_ylabel('Calculated final temperature (k)')
# ax_final.grid()
# ax_final.legend()
# fig_final.savefig('calibrate_T.svg')
# fig_final.savefig('calibrate_T.jpeg',dpi =100)


f.close()
