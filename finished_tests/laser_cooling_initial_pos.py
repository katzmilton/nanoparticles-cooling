from myfuncs import *
import matplotlib.pyplot as plt

name = 'laser_cooling'

long_onda = 1e-6

k = [2*np.pi/long_onda/np.sqrt(3)]*3

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 

gamma = 80e6
detuning = gamma/2
cloud_radius = [1e-4,1e-6]

trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha': np.pi/8}

for cloud in range(len(cloud_radius)):
    try:
        ions = {'mass': mass, 'charge': charge, 'cloud_radius': cloud_radius[cloud]}
        laser = {'gamma': gamma, 'k': k, 'detuning': detuning}

        time,positions,velocities = dopplercooling(name,ions,trap,laser,5e6)

        np.savez(f'resultados_laser_cooling_init_pos_{cloud_radius[cloud]:.2e}.npz',time= time, positions= positions, velocities= velocities,ions = ions) 


        fig,ax= plt.subplots(3,sharex =True)
        ax[0].plot(time[::10],positions[::10,0,0])
        ax[1].plot(time[::10],positions[::10,0,1])
        ax[2].plot(time[::10],positions[::10,0,2])
        ax[2].set_xlabel('Time (s)')
        ax[0].set_ylabel('X (m)')
        ax[1].set_ylabel('Y (m)')
        ax[2].set_ylabel('Z (m)')
        ax[0].grid()
        ax[1].grid()
        ax[2].grid()
        ax[0].set_title(f'resultados_laser_cooling_init_pos_{cloud_radius[cloud]:.2e}')
    except:
        f = open('fallos.txt','a')
        f.write(f'detuning = {cloud_radius[cloud]:.2e}\n\n')
        f.close()
plt.show()
