import numpy as np
from matplotlib import pyplot as plt
from analysis_functions import *
import scipy.constants as spc

gamma = 80e6
emission = [True,False]

print('leyendo resultados')
resultados_emission = dict(np.load(f'resultados_efield_emission_True.npz',allow_pickle = True))
resultados_field = dict(np.load(f'resultados_efield_emission_False.npz',allow_pickle = True))
ions = resultados_emission.get('ions',np.array({'mass': 40})).item()
macro_freq = min([frequencies(resultados_field['positions'][:,0,d],resultados_field['time'][1]-resultados_field['time'][0])[0] for d in range(3)])

steps_per_period = int((1/macro_freq)/(resultados_field['time'][1]-resultados_field['time'][0]))
print(steps_per_period)

v_emission = resultados_emission['velocities'] - resultados_field['velocities']
# v_avg_emission = np.array([runningMean(resultados_emission['velocities'][:,0,d],steps_per_period) for d in range(3)])
# v_avg_field = np.array([runningMean(resultados_field['velocities'][:,0,d],steps_per_period) for d in range(3)])
temp_emission = measure_temperature(resultados_emission['velocities'],50*steps_per_period,ions['mass'])[0]
temp_v = measure_temperature(v_emission,50*steps_per_period,ions['mass'])[0]
temp_field = measure_temperature(resultados_field['velocities'],50*steps_per_period,ions['mass'])[0]

temp_emission -= temp_field
Tfinal = [np.mean(temp_emission[-1*steps_per_period:]),np.mean(temp_field[-1*steps_per_period:])]
Tstd = [np.std(temp_emission[-1*steps_per_period:],ddof=1)/np.sqrt(len(temp_emission[-1*steps_per_period:])),np.std(temp_field[-1*steps_per_period:],ddof=1)/np.sqrt(len(temp_field[-1*steps_per_period:]))]

print('graficando temperatura')
fig_T,ax_T = plt.subplots()
ax_T.semilogy(resultados_emission['time'][:len(temp_emission)],temp_emission,label = 'con fuerza estocástica')
ax_T.semilogy(resultados_emission['time'][:len(temp_emission)],temp_v,label = 'restado el campo antes')
ax_T.semilogy(resultados_emission['time'][:len(temp_emission)][-1*steps_per_period:],Tfinal[0]*np.ones(1*steps_per_period))
# ax_T.semilogy(resultados_field['time'][:len(temp_field)],temp_field,label = 'sin fuerza estocástica')
# ax_T.semilogy(resultados_field['time'][-1*steps_per_period:],Tfinal[1]*np.ones(1*steps_per_period))
ax_T.set_xlabel('Time (s)')
ax_T.set_ylabel('Temperature (k)')
ax_T.grid()
ax_T.legend()
fig_T.savefig(f'temperatura_efield_emission.png')

print(Tfinal)

plt.show()
