import numpy as np
from analysis_functions import *
from matplotlib import pyplot as plt

nperiods = 5
T_final = np.logspace(-4,-1,4)
damp = [1e-4]

Tavg_final = [[] for i in range(len(damp))]
Tstd_final = [[] for i in range(len(damp))]
Tavg_final_ps = [[] for i in range(len(damp))]
Tstd_final_ps = [[] for i in range(len(damp))]
T_final_working = [[] for i in range(len(damp))]

f = open('datos.txt','a')

fig_T,ax_T = plt.subplots()
##
for T_i in range(len(T_final)):
    for d_i in range(len(damp)):

        f.write(f'\nSimulation with damp = {damp[d_i]:.0e} and final T = {T_final[T_i]:.0e}\n')
        resultados = load_data(f'resultados_damp_{damp[d_i]}_Tfinal_{T_final[T_i]}.npz',allow_pickle = True)
        try:
            resultados_ps = load_data(f'resultados_damp_{damp[d_i]}_Tfinal_{T_final[T_i]}_pseudo.npz')
        except:
            resultados_ps = resultados
        time = resultados['time']
        positions = resultados['positions']
        velocities = resultados['velocities']
        velocities_ps = resultados_ps['velocities']
        ions = resultados['ions']
        trap = resultados['trap']

        macro_freq = min([frequencies(positions[:,0,d],time[1]-time[0])[0] for d in range(3)])
        f.write(f'macro motion frequency  \t {macro_freq:e} Hz\n')
                
        steps_per_period = int((1/macro_freq)/(time[1]-time[0]))
        corr_steps = int(damp[d_i]/(time[1]-time[0]))

        print(steps_per_period)

        if steps_per_period < 700:
            print(f'calculando temperatura T_final = {T_final[T_i]:.0e}, damp = {damp[d_i]:.0e}')
            T_porlammps = measure_temperature(velocities,nperiods*steps_per_period,ions['mass'])[0]
            T_porlammps_ps = measure_temperature(velocities_ps,nperiods*steps_per_period,ions['mass'])[0]
            
            # print(f'calculando accion T_final = {T_final[T_i]:.0e}, damp = {damp[d_i]:.0e}')
            # s = ions['mass']*1.6e-27*action(positions,velocities,nperiods*steps_per_period)[0]/spc.hbar

            # print('graficando s ')
            fig,ax = plt.subplots()
            # ax[1].semilogy(time[:len(s)],s,label = 'action')
            print('graficando T')
            ax.semilogy(time[:len(T_porlammps)],T_porlammps,label = 'Temperature')
            ax.semilogy(time[:len(T_porlammps_ps)],T_porlammps_ps,label = 'Temperature pseudo')
            print('seteando formato')
            ax.set_xlabel('Time (s)')
            ax.set_ylabel('Temperature (k)')
            # ax[1].set_ylabel('Action / $\hbar$')
            # ax[0].legend()
            # ax[1].legend()
            fig.savefig(f'Temperature_lanT_{T_final[T_i]:.0e}_damp_{damp[d_i]:.0e}.png')

            print('otras cosas')
            T,std = final_temperature(velocities,corr_steps,ions['mass'])
            T_ps,std_ps = final_temperature(velocities_ps,corr_steps,ions['mass'])
            Tavg_final[d_i].append(T[0])
            Tstd_final[d_i].append(std[0])
            Tavg_final_ps[d_i].append(T_ps[0])
            Tstd_final_ps[d_i].append(std_ps[0])
            ax.semilogy(time[corr_steps:len(T_porlammps)],Tavg_final[d_i][-1]*np.ones(len(time[corr_steps:len(T_porlammps)])), label = 'final T')
            ax.semilogy(time[corr_steps:len(T_porlammps_ps)],Tavg_final_ps[d_i][-1]*np.ones(len(time[corr_steps:len(T_porlammps_ps)])),label = 'pseudo final T')
            ax.legend()
            f.write(f'Final temperature \t ({Tavg_final[d_i][-1]} +- {Tstd_final[d_i][-1]}) k\n')
            # f.write(f"Final action: \t {np.mean(s[-500*steps_per_period:])}\n")

            T_final_working[d_i].append(T_final[T_i])
        
        else:
            f.write(f'{steps_per_period} steps too long to average \n')

        print('graficando posiciones')
        figx,axx = plt.subplots(3,sharex =True)
        axx[0].plot(time[::],positions[::,0,0])
        axx[1].plot(time[::],positions[::,0,1])
        axx[2].plot(time[::],positions[::,0,2])
        axx[2].set_xlabel('Time (s)')
        axx[0].set_ylabel('X (m)')
        axx[1].set_ylabel('Y (m)')
        axx[2].set_ylabel('Z (m)')
        figx.savefig(f'Positions_T_{T_final[T_i]:.0e}_damp_{damp[d_i]:.0e}.png')


a,q = aq_coefficients(ions,trap)
beta = np.sqrt(a+q**2/2)
alpha = (1 + (q/(2*beta))**2 * (beta**2/4 +1))
print('graficando calibración')
fig_Tf,ax_Tf = plt.subplots()
for i in range(len(T_final_working)):
    ax_Tf.errorbar(np.array(T_final_working[i]),np.array(Tavg_final[i]), yerr = np.array(Tstd_final[i]), marker = 'o',ls= ' ', label = f'Simulación con potencial completo')
    ax_Tf.errorbar(np.array(T_final_working[i]),np.array(Tavg_final_ps[i]), yerr = np.array(Tstd_final_ps[i]), marker = 'o',ls= ' ', label = f'Simulación con pseudopotencial')

print(T_final_working)
ax_Tf.plot(T_final_working[0],T_final_working[0],ls='-',label='Valor esperado pseudopotencial')
ax_Tf.fill_between(T_final_working[0],min(alpha)*np.array(T_final_working[0]),max(alpha)*np.array(T_final_working[0]),ls='-',label='Valor esperado potencial completo',alpha = 0.5)

ax_Tf.set(xscale = 'log')
ax_Tf.set(yscale = 'log')


ax_Tf.set_xlabel('Temperatura del medio (k)')
ax_Tf.set_ylabel('Temperatura final de la partícula (k)')
ax_Tf.legend()
fig_Tf.savefig('calibrate_T.svg')
fig_Tf.savefig('calibrate_T.png')

f.close()
plt.show()
