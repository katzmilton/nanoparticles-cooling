import numpy as np
import scipy as sp
from matplotlib import pyplot as plt
from analysis_functions import *
import scipy.constants as spc

print('leyendo resultados')
damp = 1e-3
filename = f'resultados_efield_emission_1000particles_{damp:.0e}.npz'
resultados = load_data(filename)
ions = resultados['ions']
trap = resultados['trap']
time = resultados['time']
print(time[1]-time[0])

macro_freq = [frequencies(resultados['velocities'][:,0,d],resultados['time'][1]-resultados['time'][0])[0] for d in range(3)]
# macro_freq = [3e4]
steps_per_period = int(1/(min(macro_freq)*(resultados['time'][1] - resultados['time'][0])))
corr_steps = int(damp/(time[1]-time[0]))
nperiods = 10

temp_emission = measure_temperature(resultados['velocities'],nperiods*steps_per_period,ions['mass'])
Tfinal = final_temperature(resultados['velocities'],corr_steps,ions['mass'],std = False)

fig_timegas = plt.figure()
fig_temp,ax_temp = plt.subplots()
ax_timegas = fig_timegas.subplots()
# fig_timegas.tight_layout()
cant_hists = 3
fig_bootstrap,ax_bootstrap = plt.subplots(1,cant_hists,sharey = True)


ax_timegas.hist(Tfinal,bins=20, density = True,label = 'Histograma de los promedios temporales',zorder=0,alpha =0.4)


# ax_timegas.plot(np.mean(Tfinal)*np.ones(2),[0,40],ls=':',label = 'Valor medio de los datos',zorder=2, color = '#4C72B0')
# ax_hist.fill_between([np.mean(Tfinal)-np.std(Tfinal,ddof=1),np.mean(Tfinal)+np.std(Tfinal,ddof=1)],120*np.ones(2),zorder=1,alpha = 0.3)

print(np.sum(resultados['velocities'][-1,:,:]**2,axis=1).shape)
v2_avg_gas = np.mean(np.sum(resultados['velocities'][-1,:,:]**2,axis=1))
v2_std_gas = np.std(np.sum(resultados['velocities'][-1,:,:]**2,axis=1),ddof=1)
product = ions['mass']*1.66e-27/3/spc.k
Tfinal_Npart = product*v2_avg_gas
Tstd_Npart = product*v2_std_gas
ax_timegas.plot(Tfinal_Npart*np.ones(2),[0,max(np.histogram(Tfinal,bins=20,density=True)[0])],ls=':',label='Valor medio de la temperatura a tiempo fijo',zorder = 2,color = '#8172B2')
ax_timegas.fill_between([Tfinal_Npart-Tstd_Npart/np.sqrt(1000),Tfinal_Npart+Tstd_Npart/np.sqrt(1000)],max(np.histogram(Tfinal,bins=20,density=True)[0])*np.ones(2),zorder=1,alpha = 0.4,color = '#8172B2')
for j in range(cant_hists):
    i = np.random.randint(0,1000)
    bootstrap_dist = var_block_bootstrap(product*np.sum(resultados['velocities'][:,i,:]**2,axis=1),int(corr_steps),histogram = True)
    ax_bootstrap[j].hist(bootstrap_dist,bins = 20,density = True, label = 'Bootstrap de una única simulación',alpha = 0.6,color = '#55A868')
    ax_bootstrap[j].plot(Tfinal[i]*np.ones(2),[0,max(np.histogram(bootstrap_dist,bins=20,density=True)[0])], ls = '--',color = '#4C72B0', label = 'Valor medio de la simulación')
    ax_bootstrap[j].fill_between([Tfinal[i]-np.std(Tfinal),Tfinal[i]+np.std(Tfinal)], max(np.histogram(bootstrap_dist,bins=20,density=True)[0])*np.ones(2),zorder = 3,alpha = 0.4, color = '#4C72B0')
    # ax_bootstrap.plot(np.mean(bootstrap_dist)*np.ones(2),[0,40], ls = '--',color = '#55A868')
    ax_bootstrap[j].set_xlabel('Temperatura (k)')
fig_bootstrap.legend()

ax_timegas.set_xlabel('Temperatura (k)')
ax_timegas.legend()

f = open('datos.txt','a')
f.write(f"simulacion con {resultados['velocities'].shape[1]} particulas\n")
f.write(f'promedio temporal de la temperatura: {np.mean(Tfinal):.2}\n')
f.write(f'temperatura promediando como un gas: {Tfinal_Npart:.2}\n\n')
f.close()

fig_vel, ax_v = plt.subplots(3,sharex=True)
[ax_vi.plot(resultados['time'],resultados['velocities'][:,i,d]) for d,ax_vi in enumerate(ax_v)]
ax_v[0].set_title(f'velocidad de la particula {i}')
ax_temp.plot(resultados['time'][:len(temp_emission[i])],temp_emission[i,:],label='Temperatura')
ax_temp.plot(time[corr_steps:],Tfinal[i]*np.ones(len(time)-corr_steps),ls='--',alpha=0.5,label='tiempo promediado')
ax_temp.set_xlabel('Tiempo (s)')
ax_temp.set_ylabel('Temperatura (k)')
ax_temp.legend()

# ax_temp.set_title(f'Temperatura de la partícula {i}')
fig_bootstrap.savefig('bootstrap_histogram.png',dpi=800)
fig_timegas.savefig('timegas_histogram.png',dpi = 800)
fig_bootstrap.savefig('bootstrap_histogram.svg')
fig_timegas.savefig('timegas_histogram.svg')
fig_temp.savefig('temp_brown.png',dpi = 800)
fig_bootstrap.savefig('temp_brown.svg')

sigmas = []
for j in range(1000):
    i = np.random.randint(0,1000)
    sigmas.append(np.sqrt(var_block_bootstrap(product*np.sum(resultados['velocities'][:,i,:]**2,axis=1),int(corr_steps))))
fig_sigmas,ax_sigmas = plt.subplots()
ax_sigmas.plot(np.std(Tfinal)*np.ones(2), [0,max(np.histogram(sigmas,bins=20,density=True)[0])],'--',label='Desvío de los promedios temporales')
ax_sigmas.hist(sigmas,bins=20,density = True,label = 'Histograma de valores estimados por Bootstrap',alpha = 0.5)
ax_sigmas.set_xlabel('Desvío estándar (k)')
ax_sigmas.legend()
fig_sigmas.savefig('sigmas_hist.png',dpi=600)
fig_sigmas.savefig('sigmas_hist.svg')
np.savez('100sigmas.npz',sigmas = sigmas)
print(min(sigmas),max(sigmas),np.mean(sigmas))
print(np.std(Tfinal))
plt.show()
