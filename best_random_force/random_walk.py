import numpy as np
import matplotlib.pyplot as plt

def random_walk(pasos,tamano_paso,N_particulas):
    posiciones = np.zeros([3,N_particulas])
    for i in range(pasos):
        ji = np.random.random(N_particulas)*2-1
        phi = np.random.random(N_particulas)*2*np.pi
        theta = np.arccos(ji)
        delta = np.array([tamano_paso*(np.sin(theta)*np.cos(phi)), tamano_paso*(np.sin(theta)*np.sin(phi)),tamano_paso*np.cos(theta))])  
        posiciones[0] += delta[0]
        posiciones[1] += delta[1]
        posiciones[2] += delta[2]
    return posiciones,delta

tamano_paso = 1
N = 10000
pasos = 100

posiciones,delta = random_walk(pasos,tamano_paso,N)

textfile = open('datos.txt','a')
textfile.write(f'{np.mean(delta,axis=1)=}\n')
textfile.write(f'{np.cov(delta)=}\n')
textfile.write(f'{np.mean(np.sum(delta**2,axis=0))=}\n')
textfile.write(f'{np.cov(posiciones)=}\n\n')
# textfile.write(f'{np.mean(np.sum(posiciones**2,axis =0))/pasos=}\n\n')
# textfile.write(f'{np.mean(posiciones[0])=}\n')
# textfile.write(f'{np.mean(posiciones[1])=}\n')
# textfile.write(f'{np.mean(posiciones[2])=}\n')
# textfile.write(f'{np.mean(posiciones[0]**2 + posiciones[1]**2 + posiciones[2]**2)/pasos=}\n\n')
# textfile.write(f'{np.var(np.sqrt(px[-1]**2 + py[-1]**2 + pz[-1]**2))/(pasos*p)=}\n\n')
# textfile.write(f'{np.mean((np.sqrt(px[-1]**2 + py[-1]**2 + pz[-1]**2)-np.mean(np.sqrt(px[-1]**2 +py[-1]**2+pz[-1]**2)))**2)/(pasos*p)=}\n\n')
textfile.close()

# plt.hist(np.linalg.norm(posiciones,axis=0),density = True,bins = 30)
# fig_pos = plt.figure()
# ax_pos  = fig_pos.add_subplot(111,projection = '3d')
# ax_pos.scatter(posiciones[0],posiciones[1],posiciones[2],alpha = 0.5)
# ax_pos.set_box_aspect([1,1,1])

plt.show()
