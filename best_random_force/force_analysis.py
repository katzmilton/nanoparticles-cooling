import numpy as np

import matplotlib.pyplot as plt
import scipy.constants as spc

filename_data = "applied_langevin.txt"
long_onda = 1e-6
k = np.linalg.norm([2*np.pi/long_onda/np.sqrt(3)]*3)
gamma = 80e6
mass = 40*1.66e-27
dt = 1.2987012987012986e-08

gamma2 = spc.hbar*np.linalg.norm(k)*gamma
print('reading txt')
applied_forces = np.loadtxt(filename_data)
print('done')


applied_forces /= gamma2
mod_force = np.linalg.norm(applied_forces,axis=1)

print('histogram')
plt.hist(mod_force)
print('done')
print(f"{np.mean(mod_force)=}")
print(f"{np.std(mod_force,ddof = 1)=}")
print(f"{min(mod_force)=}")
print(f"{max(mod_force)=}")

plt.show()
