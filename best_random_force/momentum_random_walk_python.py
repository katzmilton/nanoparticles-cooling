import numpy as np

import scipy.constants as spc
def paso_integracion(posicion,velocidad,aceleracion,dt):
    posicion += velocidad*dt + aceleracion*dt**2/2
    velocidad += dt*aceleracion
    return [posicion,velocidad]

def fuerza(pasos,dt,p):
    a0 = np.random.rand(pasos)
    ji = 2*np.random.rand(pasos)-1
    phi = 2*np.pi*np.random.rand(pasos)
    theta = np.arccos(ji)

    heavyside = (a0 < p).astype(int)
    
    f = spc.hbar*np.linalg.norm(k)*heavyside/dt

    return [f*np.sin(theta)*np.cos(phi),f*np.sin(theta)*np.sin(phi),f*np.cos(theta)]

dt = 1.3e-10
gamma = 80e6
long_onda = 1e-6
k = [2*np.pi/long_onda/np.sqrt(3)]*3
masa = 40*1.66e-27
numero_particulas = 10000
pasos = 1000
p = 1 - np.exp(-dt*gamma)

x,y,z = [np.zeros([pasos,numero_particulas]) for i in range(3)]
vx,vy,vz = [np.zeros([pasos,numero_particulas]) for i in range(3)]
fx,fy,fz = [np.zeros([pasos,numero_particulas]) for i in range(3)]

for part in range(numero_particulas):
    fx[:,part],fy[:,part],fz[:,part] = fuerza(pasos,dt,p)

for i in range(pasos-1):
    x[i+1],vx[i+1] = paso_integracion(x[i],vx[i],fx[i]/masa,dt)
    y[i+1],vy[i+1] = paso_integracion(y[i],vy[i],fy[i]/masa,dt)
    z[i+1],vz[i+1] = paso_integracion(z[i],vz[i],fz[i]/masa,dt)

px,py,pz = [masa*v/(spc.hbar*np.linalg.norm(k)) for v in [vx,vy,vz]]

textfile = open('datos.txt','a')
textfile.write(f'{len(px[-1])=}\n')
textfile.write(f'{np.mean(px[-1])=}\n')
textfile.write(f'{np.mean(py[-1])=}\n')
textfile.write(f'{np.mean(pz[-1])=}\n')
textfile.write(f'{np.mean(px[-1]**2 + py[-1]**2 + pz[-1]**2)/(pasos*p)=}\n\n')
# textfile.write(f'{np.mean((np.sqrt(px[-1]**2 + py[-1]**2 + pz[-1]**2)-np.mean(np.sqrt(px[-1]**2 +py[-1]**2+pz[-1]**2)))**2)/(pasos*p)=}\n\n')
textfile.close()



