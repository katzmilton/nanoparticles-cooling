from sim_functions import *
import matplotlib.pyplot as plt

name = 'laser_cooling'

long_onda = 1e-6

k = 2*np.pi/long_onda/np.sqrt(3) * np.array([1,1,1])

mass = 40 #Da
charge = 0 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 

gamma = 80e6
# detuning = np.arange(1,20,2)*gamma/2
# detuning = [(gamma/2)*i for i in np.array([1,5,10,15,20])]
detuning = [5*gamma/2]
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 0, 'endcapvoltage': 0,'anisotropy' : 0.9, 'alpha':0, 'pseudo': False}
ions = {'mass': mass, 'charge': charge,'initial_positions': [[0,0,0]]}

laser = {'gamma': gamma, 
         # 'detuning': detuning[det_i], 
         # 'emission':False,
         'k': k} 

# if det_i == 1:
#     laser['emission'] = False
time,positions,velocities = dopplercooling(name,ions,trap,laser,1e7)

p = mass*1.66e-27*velocities[:,0,:]/(spc.hbar*np.linalg.norm(k))
print(np.mean(np.sum(p[-100:,:]**2,axis = 1))/((1-np.exp(-1.3e-10*gamma))*1e7))

np.savez(f'momentum_random_walk.npz',time= time, positions= positions, velocities= velocities,ions = ions) 


plt.show()
