from sim_functions import *
import matplotlib.pyplot as plt
import numpy as np

name = 'mom_rnd_wlk'

long_onda = 1e-6

k = np.array([1,1,1])* 2*np.pi/long_onda/np.sqrt(3)

mass = 40 #Da
charge = 0 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 

gamma = 80e6
# detuning = [(gamma/2)*i for i in np.arange(0.6,1.4,0.4)]
detuning = [gamma/2]*100
ions = {'mass': mass, 'charge': charge,'initial_positions': [[0,0,0]]}
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 0, 'endcapvoltage': 0,'anisotropy' : 1, 'alpha': 0, 'domain': [5e-3,5e-3,5e-3]}

final_p = []
px = []
py = []
pz = []

for det_i in range(len(detuning)):
    try:
        laser = {'gamma': gamma, 'k': k}

        time,positions,velocities = dopplercooling(name,ions,trap,laser,1000)
        px.append(mass*1.66e-27*velocities[-1,0,0]/(spc.hbar*np.linalg.norm(k)))
        py.append(mass*1.66e-27*velocities[-1,0,1]/(spc.hbar*np.linalg.norm(k)))
        pz.append(mass*1.66e-27*velocities[-1,0,2]/(spc.hbar*np.linalg.norm(k)))
        final_p.append((mass*1.6e-27)*velocities[-1,0,:]/(spc.hbar*np.linalg.norm(k)))


    except:
        f = open('fallos.txt','a')
        f.write(f'detuning = {detuning[det_i]:.2e}\n\n')
        f.close()

p = 1 - np.exp(-1.2987012987012986e-10*gamma)

px,py,pz = [np.array(pi) for pi in [px,py,pz]]
textfile = open('datos.txt','a')
textfile.write(f'{len(px)=}\n')
textfile.write(f'{np.mean(px)=}\n')
textfile.write(f'{np.mean(py)=}\n')
textfile.write(f'{np.mean(pz)=}\n')
textfile.write(f'{np.mean(px**2 + py**2 + pz**2)/(1000*p)=}\n\n')
# print(f'<p^2> = {np.mean(final_p)/(spc.hbar*np.linalg.norm(k))**2/(3*1e4*p)}')

# np.savez(f'dispersion.npz',final_p = final_p,ions = ions) 
