from functions import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['axes.grid']=True

gamma = 80e7
wavelength = 1e-6

trap = {'radius': 3.75e-3, 
        'length': 2.75e-3, 
        'kappa': 0.244,
        'frequency': 3.85e6, 
        'voltage': 500, 
        'endcapvoltage': 15,
        'anisotropy' : 0.9, 
        'alpha': 0}

ions = {'mass': 40*1.66e-27,
        'charge': 1*1.6e-19,
        'initial_positions': np.array([0,0,0])}

laser ={'gamma': gamma, 
        # 'detuning': gamma/2,
        # 'emission': False,
        # 'linear': True,
        'k': np.array([1,1,1])*2*np.pi/(wavelength*np.sqrt(3))}

simulation = {  'timestep': 1.3e-8,
                'steps': int(1e2),
                'repetitions': 100000,
                # 'variable': 'detuning',
                'laser': laser,
                # 'trap': trap,
                'ions': ions}

x,y,z,vx,vy,vz = run_simulation(simulation)

px,py,pz = [ions['mass']*v/(spc.hbar*np.linalg.norm(laser['k'])) for v in [vx,vy,vz]]
p = 1 - np.exp(-simulation['timestep']*laser['gamma'])

textfile = open('datos.txt','a')
textfile.write(f'{len(px[:,-1])=}\n')
textfile.write(f'{np.mean(px[:,-1])=}\n')
textfile.write(f'{np.mean(py[:,-1])=}\n')
textfile.write(f'{np.mean(pz[:,-1])=}\n')
textfile.write(f"{(np.mean((px[:,-1])**2 + (py[:,-1])**2 + (pz[:,-1])**2)*len(px[:,-1]-1)/len(px[:,-1]))/(simulation['steps']*p)=}\n\n")
textfile.close()
