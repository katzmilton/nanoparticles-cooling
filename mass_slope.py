import numpy as np
from analysis_functions import *
import matplotlib.pyplot as plt

data = np.load('slopes.npz')

fit2 = [0,1,2,3,4,9]
fit1 = [5,6,7]
fit0 = [8]

slopes = []
std = []
for i in range(len(data['mass'])):
    if i in fit2:
        slopes.append(data['slopes'][i][1])
        std.append(data['std_err'][i][1])
    elif i in fit1:
        slopes.append(data['slopes'][i][1])
        std.append(data['std_err'][i][1])
    elif i in fit0:
        slopes.append(data['slopes'][i][1])
        std.append(data['std_err'][i][1])
    else:
        print('ERROR')

cont_mass = np.logspace(1.6,5.8,100)
fig_slope,ax_slope = plt.subplots()
opt,cov = inverse_fit(data['mass'],slopes,[-4])
print(f"{opt=}\t{cov=}")
ax_slope.errorbar(data['mass'],slopes,yerr=std,marker = 'o',ls = ' ',label = 'Pendientes obtenidas')
ax_slope.plot(cont_mass,opt[0]/cont_mass, label = 'Ajuste $\\frac{T_0\lambda_T}{M}$')
ax_slope.set(xscale = 'log')
ax_slope.set_xlabel('Masa (Da)')
ax_slope.set_ylabel('Pendiente de la Temperatura (k/s)')
ax_slope.legend()
fig_slope.savefig('slope_mass.png',dpi = 1200)
fig_slope.savefig('slope_mass.svg')
# np.savez('slopes.npz',slopes = slopes_all, std_err = err_all, mass = mass)


plt.show()
