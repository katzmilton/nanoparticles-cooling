from sim_functions import *
from analysis_functions import *
import matplotlib.pyplot as plt


name = 'nanoparticles'

# long_onda = 1000e-9
long_onda = 1200e-9

k1 = np.array([2*np.pi/long_onda,0,0])
k2 = np.array([0,2*np.pi/long_onda,0])
k3 = np.array([0,0,2*np.pi/long_onda])

mass = 5e2 #Da

# mass = 40 #Da
# charge = 1 #e
charge = 34 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 50e5
# trap_freq = 3.85e6 

gamma = 0.3e12
# gamma = 80e6
decay_rate = 1e3
# detuning = np.arange(1,20,2)*gamma/2
# detuning = [(gamma/2)*i for i in np.array([1,5,10,15,20])]
# detuning = [gamma/2,15*gamma/2]
detuning = [gamma/2]
# trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        # 'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha':0, 'domain': [5e-3,5e-3,5e-3],'pseudo':True}
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 1000, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha':0, 'domain': [5e-3,5e-3,5e-3],'pseudo': True}
ions = {'mass': mass, 'charge': charge,'N_ions': 1e4,'initial_positions': [[0.3e-5,0.3e-5,0.3e-5]]}
# ions = {'mass': mass, 'charge': charge,'initial_positions': [[2e-6*(np.sin(trap['alpha'])+np.cos(trap['alpha'])),2e-6,2e-6*(np.sin(trap['alpha'])-np.cos(trap['alpha']))]]}

for det_i in range(len(detuning)):
    # try:
        laser = [{'gamma': gamma, 
                 'decay_rate':decay_rate,
                 'detuning': detuning[det_i], 
                 # 'linear': True,
                 # 'emission':False,
                 # 'ratio':1/2,
                 'k': ki} for ki in [k1,-k1,k2,-k2,k3,-k3]] 
        # if det_i == 1:
        #     laser['emission'] = False
        time,positions,velocities = dopplercooling(name,ions,trap,laser,2e9)

        np.savez(f'resultados_nanoparticles_detuning_{detuning[det_i]:.2e}.npz',time= time, positions= positions, velocities= velocities,ions = ions,trap = trap,laser = laser) 
        

        fig,ax= plt.subplots(3,sharex =True)
        ax[0].plot(time,positions[:,0,0])
        ax[1].plot(time,positions[:,0,1])
        ax[2].plot(time,positions[:,0,2])
        ax[2].set_xlabel('Time (s)')
        ax[0].set_ylabel('X (m)')
        ax[1].set_ylabel('Y (m)')
        ax[2].set_ylabel('Z (m)')
        ax[0].grid()
        ax[1].grid()
        ax[2].grid()

        fig.savefig(f'resultados_nanoparticles_detuning_{detuning[det_i]:.2e}.png')
    # except:
        f = open('fallos.txt','a')
        f.write(f'detuning = {detuning[det_i]:.2e}\n\n')
        f.close()
plt.show()
