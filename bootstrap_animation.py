import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
from analysis_functions import *
import scipy.constants as spc

gamma = 23e6
detuning = 15*gamma/2
nperiods = 100

resultados = load_data(f'resultados_laser_cooling_detuning_{detuning:.2e}.npz')
ions = resultados['ions']
laser = resultados['laser'].item()
trap = resultados['trap']
saturation = 1
correlation_time = gamma*ions['mass']*1.66e-27*(1+saturation+(2*detuning/gamma)**2)**2/(2*spc.hbar*saturation*detuning*laser['k'][0]**2)

steps_per_corrtime = int(correlation_time/(resultados['time'][1]-resultados['time'][0]))
macro_freq = min([frequencies(resultados['positions'][:,0,d],resultados['time'][1]-resultados['time'][0])[0] for d in range(3)])
# macro_freq = 3e4

steps_per_period = int((1/macro_freq)/(resultados['time'][1]-resultados['time'][0]))

temp = measure_temperature(resultados['velocities'],nperiods*steps_per_period,ions['mass'])[0]

time = resultados['time'][25000:len(temp)]
temp = temp[25000:]

print('graficando temperatura')
fig_T,[ax_T,ax_boot] = plt.subplots(2,sharex = True,sharey=True)
ax_T.semilogy(time,temp)
ax_T.set_ylabel('Temperatura (K)')
ax_boot.set_ylabel('Temperatura (K)')
ax_boot.set_xlabel('Tiempo (s)')

blocks = 5
bootstrap_reps = 10
block_length = int((len(temp)-steps_per_corrtime)/blocks)

frames = 120

line_sq1, = ax_T.semilogy([], [], '--',color='#55A868')
line_sq2, = ax_T.plot([], [], '--',color='#55A868')
line_sq3, = ax_T.plot([], [], '--',color='#55A868')
line_sq4, = ax_T.plot([], [], '--',color='#55A868')
line_sq5, = ax_T.plot([], [], '--',color='#55A868')
line_T1_boot, = ax_boot.semilogy([], [], '-',color='#4C72B0')
line_T2_boot, = ax_boot.semilogy([], [], '-',color='#4C72B0')
line_T3_boot, = ax_boot.semilogy([], [], '-',color='#4C72B0')
line_T4_boot, = ax_boot.semilogy([], [], '-',color='#4C72B0')
line_T5_boot, = ax_boot.semilogy([], [], '-',color='#4C72B0')

def init():
    line_sq1.set_data([],[])
    line_sq2.set_data([],[])
    line_sq3.set_data([],[])
    line_sq4.set_data([],[])
    line_sq5.set_data([],[])
    line_sq1.set_alpha(0)
    line_sq2.set_alpha(0)
    line_sq3.set_alpha(0)
    line_sq4.set_alpha(0)
    line_sq5.set_alpha(0)
    line_T1_boot.set_data([],[])
    line_T2_boot.set_data([],[])
    line_T3_boot.set_data([],[])
    line_T4_boot.set_data([],[])
    line_T5_boot.set_data([],[])
    line_T1_boot.set_alpha(0)
    line_T2_boot.set_alpha(0)
    line_T3_boot.set_alpha(0)
    line_T4_boot.set_alpha(0)
    line_T5_boot.set_alpha(0)

    return(line_sq1,line_sq2,line_sq3,line_sq4,line_sq5,line_T1_boot,line_T2_boot,line_T3_boot,line_T4_boot,line_T5_boot,)


def animate(i):
    if i == 1:
        j=0
        initial_step = np.random.randint(steps_per_corrtime,len(temp)-block_length,size = blocks)
        sqx = np.array([time[initial_step[j]],time[initial_step[j]],time[initial_step[j]+block_length],time[initial_step[j]+block_length],time[initial_step[j]]])
        altura_rectangulo = max(temp[initial_step[j]:(initial_step[j]+block_length)])
        altura_base = min(temp[initial_step[j]:(initial_step[j]+block_length)])
        sqy = np.array([altura_base,altura_rectangulo,altura_rectangulo,altura_base,altura_base])
        line_sq1.set_data(sqx,sqy)
        time_block = np.arange(steps_per_corrtime,steps_per_corrtime+block_length)*(time[1] - time[0])
        line_T1_boot.set_data(time_block,temp[initial_step[j]:(initial_step[j]+block_length)])

        j+=1
        sqx = np.array([time[initial_step[j]],time[initial_step[j]],time[initial_step[j]+block_length],time[initial_step[j]+block_length],time[initial_step[j]]])
        altura_rectangulo = max(temp[initial_step[j]:(initial_step[j]+block_length)])
        altura_base = min(temp[initial_step[j]:(initial_step[j]+block_length)])
        sqy = np.array([altura_base,altura_rectangulo,altura_rectangulo,altura_base,altura_base])
        line_sq2.set_data(sqx,sqy)
        time_block = np.arange(j*block_length+steps_per_corrtime,(j+1)*block_length+steps_per_corrtime)*(time[1] - time[0])
        line_T2_boot.set_data(time_block,temp[initial_step[j]:(initial_step[j]+block_length)])

        j+=1
        sqx = np.array([time[initial_step[j]],time[initial_step[j]],time[initial_step[j]+block_length],time[initial_step[j]+block_length],time[initial_step[j]]])
        altura_rectangulo = max(temp[initial_step[j]:(initial_step[j]+block_length)])
        altura_base = min(temp[initial_step[j]:(initial_step[j]+block_length)])
        sqy = np.array([altura_base,altura_rectangulo,altura_rectangulo,altura_base,altura_base])
        line_sq3.set_data(sqx,sqy)
        time_block = np.arange(j*block_length+steps_per_corrtime,(j+1)*block_length+steps_per_corrtime)*(time[1] - time[0])
        line_T3_boot.set_data(time_block,temp[initial_step[j]:(initial_step[j]+block_length)])

        j+=1
        sqx = np.array([time[initial_step[j]],time[initial_step[j]],time[initial_step[j]+block_length],time[initial_step[j]+block_length],time[initial_step[j]]])
        altura_rectangulo = max(temp[initial_step[j]:(initial_step[j]+block_length)])
        altura_base = min(temp[initial_step[j]:(initial_step[j]+block_length)])
        sqy = np.array([altura_base,altura_rectangulo,altura_rectangulo,altura_base,altura_base])
        line_sq4.set_data(sqx,sqy)
        time_block = np.arange(j*block_length+steps_per_corrtime,(j+1)*block_length+steps_per_corrtime)*(time[1] - time[0])
        line_T4_boot.set_data(time_block,temp[initial_step[j]:(initial_step[j]+block_length)])

        j+=1
        sqx = np.array([time[initial_step[j]],time[initial_step[j]],time[initial_step[j]+block_length],time[initial_step[j]+block_length],time[initial_step[j]]])
        altura_rectangulo = max(temp[initial_step[j]:(initial_step[j]+block_length)])
        altura_base = min(temp[initial_step[j]:(initial_step[j]+block_length)])
        sqy = np.array([altura_base,altura_rectangulo,altura_rectangulo,altura_base,altura_base])
        line_sq5.set_data(sqx,sqy)
        time_block = np.arange(j*block_length+steps_per_corrtime,(j+1)*block_length+steps_per_corrtime)*(time[1] - time[0])
        line_T5_boot.set_data(time_block,temp[initial_step[j]:(initial_step[j]+block_length)])

    elif i<=10:
        line_sq1.set_alpha(i*0.1)
    elif i<=20:
        line_T1_boot.set_alpha((i-10)*0.1)
    elif i<=30:
        line_sq2.set_alpha((i-20)*0.1)
    elif i<=40:
        line_T2_boot.set_alpha((i-30)*0.1)
    elif i<=50:
        line_sq3.set_alpha((i-40)*0.1)
    elif i<=60:
        line_T3_boot.set_alpha((i-50)*0.1)
    elif i<=70:
        line_sq4.set_alpha((i-60)*0.1)
    elif i<=80:
        line_T4_boot.set_alpha((i-70)*0.1)
    elif i<=90:
        line_sq5.set_alpha((i-80)*0.1)
    elif i<=100:
        line_T5_boot.set_alpha((i-90)*0.1)
    return(line_sq1,line_sq2,line_sq3,line_sq4,line_sq5,line_T1_boot,line_T2_boot,line_T3_boot,line_T4_boot,line_T5_boot,)

anim = animation.FuncAnimation(fig_T,animate,init_func = init, frames = frames, interval = 100, blit = True)

# print('guardando imágen')
anim.save('bootstrap.gif', writer='imagemagick')
# plt.show()
