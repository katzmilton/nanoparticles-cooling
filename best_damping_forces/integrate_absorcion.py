import numpy as np
import scipy.constants as spc

long_onda = 1e-6
gamma = 80e6
s = 1
delta = gamma/2
freq = 3.85e6 
charge = 1*1.6e-19
kappa = 0.244
ev = 15
mass = 40*1.66e-27
length = 2.75e-3
radius = 3.75e-3
voltage = 500
anisotropy = 0.9

k = (1/np.sqrt(3)) * np.array([1,1,1]) * 2 * np.pi/long_onda
ar = -4 * charge * kappa * ev / (mass * length**2 * (2*np.pi * freq)**2)
az = -2*ar
qx = 2 * charge * voltage / (mass * radius**2 * (2*np.pi * freq)**2)
qy = -anisotropy * qx
qz = (anisotropy - 1) * qx
wx = 2*np.pi * freq / 2 * np.sqrt(ar + qx**2 / 2)
wy = 2*np.pi * freq / 2 * np.sqrt(ar + qy**2 / 2)
wz = 2*np.pi * freq / 2 * np.sqrt(az + qz**2 / 2)

T = 2*np.pi/wx
t=np.linspace(0,T,10000)
v = np.array([wx * np.cos(wx*t),wy * np.cos(wy*t),wz * np.cos(wz*t)])

rho_ee = (s/2)/(1+s+(2*(delta+k[0]*v[0] + k[1]*v[1] + k[2]*v[2])/gamma)**2)

F_a = spc.hbar*gamma*np.array([k[0]*rho_ee,k[1]*rho_ee,k[2]*rho_ee])
F0_kappa = 4*np.linalg.norm(k)**2*delta*(s/2)/(gamma*(1+s+(2*delta/gamma)**2)**2)

vporF = np.mean(F_a[0]*v[0] + F_a[1]*v[1] +F_a[2]*v[2])
vporFkappa = np.mean(F0_kappa*np.linalg.norm(v)**2)

print(f'{vporF=}')
print(f'{vporFkappa=}')
