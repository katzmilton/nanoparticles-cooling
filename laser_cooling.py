from sim_functions import *
import matplotlib.pyplot as plt

name = 'laser_cooling_noemission'

long_onda = 397e-9

k = [2*np.pi/long_onda/np.sqrt(3)]*3

mass = 40 #Da
charge = 1 #e

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 

gamma = 23e6
# detuning = np.arange(1,20,2)*gamma/2
detuning = [(gamma/2)*i for i in np.array([1,3,7,12,17])]
# detuning = [20*gamma/2,15*gamma/2]
# detuning = [gamma/2]
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 300, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha':0, 'domain': [5e-3,5e-3,5e-3], 'pseudo': True}
ions = {'mass': mass, 'charge': charge,'initial_positions': [[2e-6*(np.sin(trap['alpha'])+np.cos(trap['alpha'])),2e-6,2e-6*(np.sin(trap['alpha'])-np.cos(trap['alpha']))]]}

for det_i in range(len(detuning)):
    try:
        laser = {'gamma': gamma, 
                 'detuning': detuning[det_i], 
                 # 'linear': True,
                 # 'emission':False,
                 'k': k} 

        # if det_i == 1:
        #     laser['emission'] = False
        trap.update(pseudo = True)
        time,positions,velocities = dopplercooling(name,ions,trap,laser,5e8)

        np.savez(f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}_pseudo.npz',time= time, positions= positions, velocities= velocities,ions = ions,trap=trap,laser = laser) 
        trap.update(pseudo = False)
        time,positions,velocities = dopplercooling(name,ions,trap,laser,5e8)

        np.savez(f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}.npz',time= time, positions= positions, velocities= velocities,ions = ions,trap=trap,laser = laser) 


        # fig,ax= plt.subplots(3,sharex =True)
        # ax[0].plot(time,positions[:,0,0])
        # ax[1].plot(time,positions[:,0,1])
        # ax[2].plot(time,positions[:,0,2])
        # ax[2].set_xlabel('Time (s)')
        # ax[0].set_ylabel('X (m)')
        # ax[1].set_ylabel('Y (m)')
        # ax[2].set_ylabel('Z (m)')
        # ax[0].grid()
        # ax[1].grid()
        # ax[2].grid()
        # ax[0].set_title(f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}')

        # fig.savefig(f'resultados_laser_cooling_detuning_{detuning[det_i]:.2e}.png')
    except:
        f = open('fallos.txt','a')
        f.write(f'detuning = {detuning[det_i]:.2e}\n\n')
        f.close()
plt.show()
