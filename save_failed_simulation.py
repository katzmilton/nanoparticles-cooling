from analysis_functions import *
import matplotlib.pyplot as plt

timestep = 4.636126500405174e-09

long_onda = 1200e-9
k = [2*np.pi/long_onda/np.sqrt(3)]*3
mass = 400 #Da
charge = 34 #e
trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 50e5   
gamma = 0.3e12
decay_rate = 1e3
detuning = gamma/2
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 1000, 'endcapvoltage': 15,'anisotropy' : 0.9, 'alpha':0, 'domain': [5e-3,5e-3,5e-3],'pseudo': True}
ions = {'mass': mass, 'charge': charge,'N_ions': 1e4,'initial_positions': [[0.3e-5,0.3e-5,0.3e-5]]}

print('reading positions')
# time,positions,velocities = read_failed_simulation()
time, positions = pl.readdump('positions.txt')
print('finished reading')
time *= timestep

# print('saving positions')
# np.savez(f'positions_laser_cooling_detuning_{detuning:.2e}.npz',positions= positions,time = time)

# print('plotting')
# fig,ax= plt.subplots(3,sharex =True)
# ax[0].plot(time[::10000],positions[::10000,0,0])
# ax[1].plot(time[::10000],positions[::10000,0,1])
# ax[2].plot(time[::10000],positions[::10000,0,2])
# ax[2].set_xlabel('Time (s)')
# ax[0].set_ylabel('X (m)')
# ax[1].set_ylabel('Y (m)')
# ax[2].set_ylabel('Z (m)')
# ax[0].grid()
# ax[1].grid()
# ax[2].grid()
# ax[0].set_title(f'resultados_laser_cooling_detuning_{detuning:.2e}')

# plt.show()

print('reading velocities')
_, velocities = pl.readdump('velocities.txt')

# print('saving velocities')
# np.savez(f'velocities_laser_cooling_detuning_{detuning:.2e}.npz',velocities= velocities)

print('saving all')
np.savez(f'resultados_nanoparticles_detuning_{detuning:.2e}.npz',time= time,positions = positions,velocities = velocities, trap=trap, ions=ions)


