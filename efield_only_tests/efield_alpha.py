from functions import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['axes.grid']=True

gamma = 80e6
wavelength = 1e-6

trap = {'radius': 3.75e-3, 
        'length': 2.75e-3, 
        'kappa': 0.244,
        'frequency': 3.85e6, 
        'voltage': 500, 
        'endcapvoltage': 15,
        'anisotropy' : 0.9, 
        'alpha': np.arange(0,8,1)*2*np.pi/8}

ions = {'mass': 40*1.66e-27,
        'charge': 1*1.6e-19,
        'initial_positions': np.array([1e-4*(np.sin(trap['alpha'])+np.cos(trap['alpha'])),1e-4*np.ones(len(trap['alpha'])),1e-4*(np.sin(trap['alpha'])-np.cos(trap['alpha']))]).T}

laser ={'gamma': gamma, 
        'detuning': gamma/2,
        'emission': False,
        'k': np.array([1,1,1])*2*np.pi/(wavelength*np.sqrt(3))}

simulation = {  'timestep': 1 / np.max(trap['frequency'])/200,
                'steps': int(5e4),
                # 'repetitions': 5,
                'variable': 'alpha',
                'ions': ions,
                # 'laser': laser,
                'trap': trap}

x,y,z,vx,vy,vz = run_simulation(simulation)

np.savez('results_diff_alpha.npz',x=x,y=y,z=z,vx=vx,vy=vy,vz=vz,simulation=simulation)
for i in range(len(x)):
    freqx = frequencies(x[i],simulation['timestep'])
    freqy = frequencies(y[i],simulation['timestep'])
    freqz = frequencies(z[i],simulation['timestep'])

    ax = -4*ions['charge']*trap['kappa']*trap['endcapvoltage']/(ions['mass']*(trap['length']*trap['frequency']*2*np.pi)**2)
    qx = 2*ions['charge']*trap['voltage']/(ions['mass']*(trap['radius']*trap['frequency']*2*np.pi)**2)

    textfile = open('datos.txt','a')
    textfile.write(f"{trap['alpha'][i]=}\n")
    textfile.write(f'{freqx=}\n')
    textfile.write(f'{freqy=}\n')
    textfile.write(f'{freqz=}\n')
    textfile.write(f"{trap['frequency']=}\n")
    textfile.write(f"{trap['frequency']*np.sqrt(ax+0.5*qx**2)/2=}\n\n")
    textfile.close

    t = np.linspace(0,simulation['timestep']*simulation['steps'],simulation['steps'])
    fig_pos,[ax_x,ax_y,ax_z] = plt.subplots(3,sharex=True,figsize = (9,6))
    fig_pos.suptitle(f"Positions alpha={trap['alpha'][i]:.2e}")
    ax_x.plot(t,x[i])
    ax_y.plot(t,y[i])
    ax_z.plot(t,z[i])
    ax_z.set_xlabel('time (s)')
    ax_x.set_ylabel("X (m)")
    ax_y.set_ylabel("Y (m)")
    ax_z.set_ylabel("Z (m)")

    fig_freqs,[ax_fx,ax_fy,ax_fz] = plt.subplots(3,sharex=True)
    fig_freqs.suptitle(f"Fourier Transform of positions alpha={trap['alpha'][i]:.2e}")
    ax_fx.plot(np.fft.rfftfreq(len(x[i]),simulation['timestep']),abs(np.fft.rfft(x[i])))
    ax_fy.plot(np.fft.rfftfreq(len(y[i]),simulation['timestep']),abs(np.fft.rfft(y[i])))
    ax_fz.plot(np.fft.rfftfreq(len(z[i]),simulation['timestep']),abs(np.fft.rfft(z[i])))
    ax_fz.set_xlabel('frequency (Hz)')

    fig_pos.savefig(f"position_alpha_{trap['alpha'][i]:.2e}.png")
    fig_freqs.savefig(f"frequencies_alpha_{trap['alpha'][i]:.2e}.png")

plt.show()
