import numpy as np
from matplotlib import pyplot as plt

def paso_integracion(posicion,velocidad,aceleracion,dt):
    velocidad += 0.5*dt*aceleracion[0]
    posicion += velocidad*dt + aceleracion[0]*dt**2/2
    velocidad += 0.5*dt*aceleracion[0]
    return [posicion,velocidad]

def rftrap(trap):
    ev = trap['endcapvoltage']
    radius = trap['radius']
    length = trap['length']
    kappa = trap['kappa']
    anisotropy = trap.get('anisotropy', 1)
    offset = trap.get('offset', (0, 0))
    alpha = trap.get('alpha',0)

    voltages = []
    freqs = []
    if hasattr(trap['voltage'], '__iter__'):
        voltages.extend(trap['voltage'])
        freqs.extend(trap['frequency'])
    else:
        voltages.append(trap['voltage'])
        freqs.append(trap['frequency'])
    
    # Offset
    if offset[0] == 0:
        xpos = 'x'
    else:
        xpos = f'(x - {offset[0]:e})'

    if offset[1] == 0:
        ypos = 'y'
    else:
        ypos = f'(y - {offset[1]:e})'
    zpos = 'z'

    #Rotation
    if alpha != 0:
        xrot = f'({xpos}*{np.cos(alpha)}-{zpos}*{np.sin(alpha)})'
        zpos = f'({zpos}*{np.cos(alpha)}+{xpos}*{np.sin(alpha)})'
        xpos = xrot

    stat_const = kappa*ev/length**2

    phase = []
    E_x = []
    E_y = []

    for i, (v, f) in enumerate(zip(voltages, freqs)):
        phase = f'{2*np.pi*f}*t'
        E_x.append(f'{v}*np.cos({phase})*{xpos}/{radius**2} + {stat_const}*{xpos}')
        E_y.append(f'{-v*anisotropy}*np.cos({phase})*{ypos}/{radius**2} + {stat_const}*{ypos}')

    E_x = '+'.join(E_x)
    E_y = '+'.join(E_y)
    E_z = f'{-2*stat_const}*{zpos}'

    return [E_x,E_y,E_z] 


mass = 40*1.66e-27
charge = 1*1.6e-19 

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 
alpha = 0

ions = {'mass': mass, 'charge': charge,'initial_positions': [1e-4*(np.sin(alpha)+np.cos(alpha)),1e-4,1e-4*(np.sin(alpha)-np.cos(alpha))]}
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 1, 'alpha': alpha}

dt = 1 / np.max(trap['frequency']) / 20
pasos = int(1e4)

pos_x,pos_y,pos_z = [np.zeros(pasos) for i in range(3)]
vx,vy,vz = [np.zeros(pasos) for i in range(3)]

pos_x[0],pos_y[0],pos_z[0] = ions['initial_positions']
ex,ey,ez = rftrap(trap)

for i in range(pasos-1):
    x,y,z = [pos_x[i],pos_y[i],pos_z[i]]

    t = i*dt
    ax,ay,az = [charge*eval(ei)/mass for ei in [ex,ey,ez]]
    t = (i+1)*dt
    ax_sig,ay_sig,az_sig = [charge*eval(ei)/mass for ei in [ex,ey,ez]]

    pos_x[i+1],vx[i+1] = paso_integracion(pos_x[i],vx[i],[ax,ax_sig],dt)
    pos_y[i+1],vy[i+1] = paso_integracion(pos_y[i],vy[i],[ay,ay_sig],dt)
    pos_z[i+1],vz[i+1] = paso_integracion(pos_z[i],vz[i],[az,az_sig],dt)

t = np.linspace(0,dt*pasos,pasos)
fig_pos,[ax_x,ax_y,ax_z] = plt.subplots(3,sharex=True)
ax_x.plot(t,pos_x)
ax_y.plot(t,pos_y)
ax_z.plot(t,pos_z)

plt.show()
