from myfuncs import *
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as spc

name = Path(__file__).stem 

mass = 40 #Da
charge = 1 #e
gamma = 80e6
wavelength = 1e-6

trap_radius = 3.75e-3
trap_length = 2.75e-3
trap_kappa  = 0.244
trap_freq = 3.85e6 
x0 = [[1e-4,1e-4,1e-4]]

ions = {'mass': mass, 'charge': charge, 'initial_positions': x0} 
trap = {'radius': trap_radius, 'length': trap_length, 'kappa': trap_kappa,
        'frequency': trap_freq, 'voltage': 500, 'endcapvoltage': 15,'anisotropy' : 0.9,'alpha': 0}

Tavg_final = []

laser ={'gamma': gamma, 
        'detuning': gamma/2,
        'emission': False,
        'k': -np.array([1,1,1])*2*np.pi/(wavelength*np.sqrt(3))}

time,positions,velocities = dopplercooling(name,ions,trap,laser,1e7)
# np.savez(f'resultados_damp_{damp[d_i]:.0e}',time=time,positions=positions,velocities=velocities,ions = ions)

t = time
x = positions[:,0,0]
y = positions[:,0,1]
z = positions[:,0,2]

# freqx = frequencies(x,t[1]-t[0])
# freqy = frequencies(y,t[1]-t[0])
# freqz = frequencies(z,t[1]-t[0])

# print(f'{freqx=}')
# print(f'{freqy=}')
# print(f'{freqz=}')
# print(f"{trap['frequency']=}")

# ax = -4*ions['charge']*1.6e-19*trap['kappa']*trap['endcapvoltage']/(ions['mass']*1.66e-27*(trap['length']*trap['frequency']*2*np.pi)**2)
# qx = 2*ions['charge']*1.6e-19*trap['voltage']/(ions['mass']*1.66e-27*(trap['radius']*trap['frequency']*2*np.pi)**2)
# print(f'{ax=}')
# print(f'{qx=}')
# print(f"{trap['frequency']*np.sqrt(ax + 0.5*qx**2)/2=}")

fig_pos,[ax_x,ax_y,ax_z] = plt.subplots(3,sharex=True)
ax_x.plot(t[::1],x[::1])
ax_y.plot(t[::1],y[::1])
ax_z.plot(t[::1],z[::1])

# fig_freqs,[ax_fx,ax_fy,ax_fz] = plt.subplots(3,sharex=True)
# ax_fx.plot(np.fft.rfftfreq(len(x),time[1]-time[0]),abs(np.fft.rfft(x)))
# ax_fy.plot(np.fft.rfftfreq(len(y),time[1]-time[0]),abs(np.fft.rfft(y)))
# ax_fz.plot(np.fft.rfftfreq(len(z),time[1]-time[0]),abs(np.fft.rfft(z)))

# print(f"{len(x.shape)=}")

plt.show()

# textfile = open('datos.txt','a')
# textfile.write(f'{freqx=}\n')
# textfile.write(f'{freqy=}\n')
# textfile.write(f'{freqz=}\n')
# textfile.write(f"{trap['frequency']=}\n")
# textfile.write(f"{trap['frequency']*np.sqrt(ax+0.5*qx**2)/2=}\n\n")
# textfile.close

fig_pos.savefig(f"lammps_position_dt_1.3e-10_steps_1e7_damped.png")
# fig_freqs.savefig(f"frequencies_dt_1.3e-9.png")
