from functions import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcParams['axes.grid']=True

gamma = 80e6
wavelength = 1e-6

trap = {'radius': 3.75e-3, 
        'length': 2.75e-3, 
        'kappa': 0.244,
        'frequency': 3.85e6, 
        'voltage': 500, 
        'endcapvoltage': 15,
        'anisotropy' : 0.9, 
        'alpha': np.pi/8}

ions = {'mass': 40*1.66e-27,
        'charge': 1*1.6e-19,
        'initial_positions': np.array([1e-5*(np.sin(trap['alpha'])+np.cos(trap['alpha'])),1e-5,1e-5*(np.sin(trap['alpha'])-np.cos(trap['alpha']))])}

laser ={'gamma': gamma, 
        'detuning': gamma/2,
        # 'emission': False,
        # 'linear': True,
        'k': np.array([1,1,1])*2*np.pi/(wavelength*np.sqrt(3))}

simulation = {  'timestep': 1 / np.max(trap['frequency'])/20000,
                'steps': int(1e9),
                # 'repetitions': 5,
                # 'variable': 'detuning',
                'save_every': 1e3,
                'laser': laser,
                'trap': trap,
                'ions': ions}

x,y,z,vx,vy,vz = run_simulation(simulation)

t = np.linspace(0,simulation['timestep']*simulation['steps'],int(simulation['steps']/simulation['save_every']))
np.savez(f"python_results_damping_dt_{simulation['timestep']:.2e}_steps_{simulation['steps']:.0e}.npz",t = t, x=x,y=y,z=z,vx=vx,vy=vy,vz=vz,simulation=simulation)

fig_pos,[ax_x,ax_y,ax_z] = plt.subplots(3,sharex=True,figsize = (9,6))
ax_x.plot(t,x)
ax_y.plot(t,y)
ax_z.plot(t,z)
ax_z.set_xlabel('time (s)')
ax_x.set_ylabel("X (m)")
ax_y.set_ylabel("Y (m)")
ax_z.set_ylabel("Z (m)")

fig_pos.savefig(f"python_position_detuning_{laser['detuning']:.2e}.png")

plt.show()

# for i in range(len(x)):
#     t = np.linspace(0,simulation['timestep']*simulation['steps'],simulation['steps'])
#     fig_pos,[ax_x,ax_y,ax_z] = plt.subplots(3,sharex=True,figsize = (9,6))
#     ax_x.plot(t,x[i])
#     ax_y.plot(t,y[i])
#     ax_z.plot(t,z[i])
#     ax_z.set_xlabel('time (s)')
#     ax_x.set_ylabel("X (m)")
#     ax_y.set_ylabel("Y (m)")
#     ax_z.set_ylabel("Z (m)")

#     fig_pos.savefig(f"position_detuning_{laser['detuning'][i]:.2e}.png")

# plt.show()
