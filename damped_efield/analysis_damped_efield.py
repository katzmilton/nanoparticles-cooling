import numpy as np
from matplotlib import pyplot as plt

# filename = 'results_damping_dt_1.30e-11_steps_5e+08.npz'
filename = 'results_efield.npz'
results = np.load(filename,allow_pickle = True)
simulation = results['simulation'].item()
ions = simulation['ions']
trap = simulation['trap']

potential = ions['charge']*((trap['voltage']/(2*trap['radius']**2))*(results['x']**2 - trap['anisotropy']*results['y']**2 + (trap['anisotropy'] - 1)*results['z']**2)*np.cos(2*np.pi*trap['frequency']*results['t']) - (trap['endcapvoltage']*trap['kappa']/(2*trap['length']**2))*(2*results['z']**2-results['x']**2-results['y']**2))

kinetic = 0.5*ions['mass']*(results['vx']**2 + results['vy']**2 + results['vz']**2)

energy = kinetic + potential

fig_energy,ax_energy = plt.subplots()
ax_energy.plot(results['t'],energy)
ax_energy.grid()
ax_energy.set_xlabel('time (s)')
ax_energy.set_ylabel('energy (J)')

plt.show()
