import pylion as pl
import numpy as np
import scipy.constants as spc
from scipy import signal as sg
import scipy.optimize as op


def read_failed_simulation():

    steps, positions = pl.readdump('positions.txt')

    steps, velocities = pl.readdump('velocities.txt')

    return [steps,positions,velocities]

def measure_temperature(velocities,nsteps,mass =1):
    temp = []
    if len(velocities.shape) == 3:
        for i in range(velocities.shape[1]):
            vel2 = (velocities[:,i,:])**2

            product = mass*1.66e-27/3/spc.k

            temp.append(np.sum([product*runningMean_fft(vel2[:,d],nsteps) for d in range(3)],axis = 0))
    else:
        vel2 = (velocities)**2

        product = mass*1.66e-27/3/spc.k

        temp.append(np.sum([product*runningMean_fft(vel2[:,d],nsteps) for d in range(3)],axis = 0))

    return np.array(temp)

def var_block_bootstrap(series,correlation_steps,bootstrap_reps = 1000,histogram = False):
# blocks = int(len(series)/correlation_steps - 1)
    blocks = 15
    block_length = int((len(series)-correlation_steps)/blocks)
# initial_step = np.random.randint(correlation_steps,len(series)-correlation_steps,size = bootstrap_reps*blocks)
    initial_step = np.random.randint(correlation_steps,len(series)-block_length,size = bootstrap_reps*blocks)
# block_mean = [np.mean(series[i:(i+correlation_steps)]) for i in initial_step]
    block_mean = [np.mean(series[i:(i+block_length)]) for i in initial_step]
    bootstrap_series_mean = [np.mean(block_mean[(blocks*i):(blocks*(i+1))]) for i in range(bootstrap_reps)]
    if histogram:
        return bootstrap_series_mean
    else:
        return np.var(bootstrap_series_mean,ddof=1)


        

def final_temperature(velocities,correlation_steps, mass = 1,std = True,bootstrap_reps = 1000):
    temp = []
    temp_std = []
    if len(velocities.shape) == 3:
        for i in range(velocities.shape[1]):
            vel2 = (velocities[correlation_steps:,i,:])**2
            product = mass*1.66e-27/3/spc.k
            temp.append(np.sum([product*np.mean(vel2[:,d]) for d in range(3)], axis = 0))
    else:
        vel2 = velocities**2
        product = mass*1.66e-27/3/spc.k
        temp.append(np.sum([product*np.mean(vel2[correlation_steps:,d]) for d in range(3)], axis = 0))

    if std:
        temp_std.append(np.sqrt(np.sum([var_block_bootstrap(product*vel2[:,d],correlation_steps,bootstrap_reps=bootstrap_reps) for d in range(3)])))
        return np.array(temp),np.array(temp_std)
    else:
        return np.array(temp)
    
def energy(velocities, positions, trap, ions):
    if len(velocities.shape) == 3:
        for i in range(velocities.shape[1]):
            a,q = aq_coefficients(ions,trap)
            omega = np.pi*trap['frequency']*np.sqrt(a+0.5*q**2)
            k = 0.5*ions['mass']*np.sum(velocities[:,i,:]**2,axis=1)
            v = 0.5*ions['mass']*np.sum([omega[d]**2*positions[:,i,d]**2 for d in range(3)])
            return k+v

def envelope(signal):
   indexes,values = sg.find_peaks(signal,height = max(signal)/2)
   return indexes,values

def runningMean(x, N):
    '''
    compute the mean of an N-size window of the x array "moving" that window across the array
    '''
    return np.convolve(x, np.ones((N,))/N)[N-1:-(N-1)] 

def runningMean_fft(x,N):
    x_fft = np.fft.rfft(x)
    window_fft = np.fft.rfft(np.ones(N)/N,len(x))
    return abs(np.fft.irfft(x_fft*window_fft))

def action(x,v,nsteps):
    '''
    compute action per mass unit
    '''
    S = []
    for i in range(x.shape[1]):
        delta_x = np.sum([runningMean_fft(x[:,i,d]**2,nsteps) for d in range(3)], axis = 0)
        delta_v = np.sum([runningMean_fft(v[:,i,d]**2,nsteps) for d in range(3)], axis = 0)
        S.append(np.sqrt(delta_v*delta_x))
    return np.array(S) 


def frequencies(signal,delta_time):
    '''
    returns an array with the main frequencies of a real signal ordered by intensity descending
    '''

    fft_signal = abs(np.fft.rfft(signal))
    indexes,_ = sg.find_peaks(fft_signal,height = max(fft_signal)/10)

    freqs = np.fft.rfftfreq(len(signal),delta_time)
    ord_freq = freqs[indexes[fft_signal[indexes].argsort()]][::-1]
    return ord_freq

def load_data(filename,*args,**kwargs):
    print('leyendo resultados')
    results = dict(np.load(filename,allow_pickle = True))
    trap = results.get('trap',np.array({
            'radius': 3.75e-3,
            'length': 2.75e-3,
            'kappa': 0.244,
            'frequency': 3.85e6,
            'voltage': 300,
            'endcapvoltage': 15,
            'anisotropy' : 0.9,
            'alpha':0,
            'domain': [5e-3,5e-3,5e-3]})).item()
    ions = results.get('ions',np.array({'mass': 40,
            'charge': 1,
            'initial_positions': [[2e-5,2e-5,2e-5]]})).item()
# laser = results.get('laser',np.array({'gamma': 80e6,
# 'k':[2*np.pi/1e-6/np.sqrt(3)]*3})).item()
    
    if kwargs.get('trap'):
        trap.update(kwargs.get('trap'))
    if kwargs.get('ions'):
        ions.update(kwargs.get('ions'))
# if kwargs.get('laser'):
# laser.update(kwargs.get('laser'))

    results.update({'trap':trap,'ions':ions})#, 'laser':laser})
    return results
    
def aq_coefficients(ions,trap):

    a = np.array([1,1,-2])*(-4*ions['charge']*1.6e-19*trap['kappa']*trap['endcapvoltage']/(ions['mass']*1.66e-27*trap['length']**2*(2*np.pi*trap['frequency'])**2))
    q = np.array([1,-trap['anisotropy'],trap['anisotropy'] - 1]) * 2*ions['charge']*1.6e-19*trap['voltage']/(ions['mass']*1.66e-27*trap['radius']**2*(2*np.pi*trap['frequency'])**2)

    return a,q

def lowpassfilter(signal,cutfreq,dt,divide = True):
    signal_fft = np.fft.rfft(signal)
    signal_freq = np.fft.rfftfreq(len(signal),dt)
    if divide:
        high_fft = np.zeros(len(signal_fft)) + 1j*np.zeros(len(signal_fft))
        high_fft[np.where(signal_freq > cutfreq)[0]] = signal_fft[np.where(signal_freq > cutfreq)[0]] 
    signal_fft[np.where(signal_freq > cutfreq)[0]] = 0
    low_signal = np.fft.irfft(signal_fft)
    if divide:
        high_signal = np.fft.irfft(high_fft)
        return low_signal,high_signal
    return low_signal

def bandpass_filter(signal,central_freq,width,dt,divide = True):
    signal_fft = np.fft.rfft(signal)
    signal_freq = np.fft.rfftfreq(len(signal),dt)
    min_freq = central_freq - width/2
    max_freq = central_freq - width/2
    if divide:
        rejection_fft = np.zeros(len(signal_fft)) + 1j*np.zeros(len(signal_fft))
        rejection_fft[np.where((signal_freq > max_freq) | (signal_freq < min_freq))[0]] = signal_fft[np.where((signal_freq > max_freq) | (signal_freq < min_freq))[0]] 
    signal_fft[np.where((signal_freq < max_freq) | (signal_freq > min_freq))[0]] = 0
    pass_signal = np.fft.irfft(signal_fft)
    if divide:
        rejection_signal = np.fft.irfft(rejection_fft)
        return pass_signal,rejection_signal
    return low_signal

def inverse_fit(datos_x,datos_y,init):
    def modelo(m,A):
        return A/m

    opt,cov = op.curve_fit(modelo,datos_x,datos_y,p0=init)
    return opt,cov
